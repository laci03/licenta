
import tensorflow as tf
import numpy as np
import sys
import retrieveData as rd
import cv2
import os
from datetime import datetime

def main(name,  fps=30.0, path="../../../Licenta_date/data/interim/original/"):
    startTime = datetime.now()
    createVideo(name, fps, path)
    print("Time taken:", datetime.now() - startTime)


def createVideo(name, fps=30.0, folderPath="../../../Licenta_date/data/interim/original/"):
    (picture_files, classes) = rd.retrieveLabeledData(folderPath)
    (allPaths, data, label) = zip(*picture_files)
    config = tf.ConfigProto(allow_soft_placement=True)
    saver = tf.train.Saver()
    data = [x for x in data]
    classes = [x for x in classes]
    allPaths = [x for x in allPaths]

    img1 = cv2.imread(allPaths[0]+data[0])
    height, width, layers = img1.shape
    fourcc = cv2.VideoWriter_fourcc(*'DIVX')
    video = cv2.VideoWriter(name, fourcc, fps, (width, height))
    counter = 0
    last_percent = 0
    current_percent = 0
    length = len(data)
    font = cv2.FONT_HERSHEY_SIMPLEX
    bottom_left_corner_of_text = (400, 100)
    font_scale = 5
    fontColor = (0, 0, 255)
    lineType = 4

    with tf.Session(config=config) as sess:
        # Restore variables from disk.
        sess.run(init)
        saver.restore(sess, "./model1.ckpt")
        print("Model restored.")
        for path, data in zip(allPaths, data):
            if(counter % 30 == 0 ):
                current_percent = counter / length
                if(last_percent*100 + 1 < current_percent*100):
                    os.system('cls' if os.name == 'nt' else 'clear')
                    print("{0}%".format(int(current_percent*100)))
                    last_percent = current_percent
                img = cv2.imread(path+data)

                probabilities = sess.run(prediction, feed_dict={
                                        X: getImage(path+data).eval()})[0]
                _, max_idx = max([(v, i) for i, v in enumerate(probabilities)])

                cv2.putText(img, classes[max_idx],
                            bottom_left_corner_of_text,
                            font,
                            font_scale,
                            fontColor,
                            lineType)
                video.write(img)

                cv2.destroyAllWindows()
            counter = counter + 1
        video.release()

    print("Done")


if __name__ == '__main__':
    main(sys.argv[1])
