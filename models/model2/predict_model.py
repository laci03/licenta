import tensorflow as tf
import numpy as np
import sys
import retrieveData as rd
import cv2
import os
import details as ds
from datetime import datetime

height, width, channels = ds.getSize()
n_outputs = ds.getNrOfOutputs()


def getImage(path):
    image_string = tf.read_file(path)
    image_decoded = tf.image.decode_jpeg(image_string)
    image_resized = tf.image.resize_images(image_decoded, [height, width])
    image_resized.set_shape([height, width, channels])
    return tf.expand_dims(image_resized, 0)


tf.reset_default_graph()


def conv2d(x, filters, ksize, name, strides=1):
    x = tf.layers.conv2d(x, filters=filters, kernel_size=ksize,
                         strides=strides, padding="SAME",
                         activation=tf.nn.relu, name=name)
    return x


with tf.device("/gpu:0"):
    X = tf.placeholder(tf.float32, shape=[
                       1, height, width, channels], name="X")

    conv1 = conv2d(X, 64, 3, "conv1")

    pool2 = tf.nn.max_pool(conv1,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 1, 1, 1],
                           padding="VALID")

    conv2 = conv2d(pool2, 128, 3, "conv2", 2)

    pool2 = tf.nn.max_pool(conv2,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 2, 2, 1],
                           padding="VALID")

    conv3 = conv2d(pool2, 64, 4, "conv3", 3)

    pool3 = tf.nn.max_pool(conv3,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 1, 1, 1],
                           padding="VALID")

    pool3_flat = tf.reshape(pool3, shape=[-1, 64*8*8])

    fullyconn1 = tf.layers.dense(
        pool3_flat, 64, activation=tf.nn.relu, name="fc1")
    fullyconn2 = tf.layers.dense(
        fullyconn1, 64, activation=tf.nn.relu, name="fc2")

    logits = tf.layers.dense(fullyconn2, n_outputs, name="output")

    prediction = tf.nn.softmax(logits)
    init = tf.global_variables_initializer()


def main(path):
    config = tf.ConfigProto(allow_soft_placement=True)
    saver = tf.train.Saver()
    with tf.Session(config=config) as sess:
        # Restore variables from disk.
        sess.run(init)
        saver.restore(sess, "./streets.ckpt")
        print("Model restored.")
        print(sess.run(prediction, feed_dict={X: getImage(path).eval()}))




if __name__ == '__main__':
    main(sys.argv[1])
