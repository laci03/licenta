import matplotlib.pyplot as plt
import os
import sys
import tensorflow as tf
import details as ds



height, width, channels = ds.getSize()
def retrieveLabeledData(dir):
    streets = os.listdir(dir)

    # Retrieving picture names
    picture_files = []
    for street in streets:
        images = [(dir + "{0}/".format(street), name, street) for name in os.listdir(dir + "{0}/".format(street)) if name [-4:] == '.jpg']
        picture_files.extend(images)
    print('Done retrieving labeled data!')
    return picture_files, streets

def decodeAndResize(path, filename, label):
    image_string = tf.read_file(path + filename)
    image_decoded = tf.image.decode_jpeg(image_string, channels=channels)
    image_resized = tf.image.resize_images(image_decoded, [height, width])
    image_resized.set_shape([height, width, channels])
    return image_resized, label
