import tensorflow as tf
import retrieveData as rd
import details as ds
from datetime import datetime

datasetPath = ds.getDatasetPath()

def conv2d(x, filters, ksize, name, strides=1):
    x = tf.layers.conv2d(x, filters=filters, kernel_size=ksize,
                        strides=strides, padding="SAME",
                        activation=tf.nn.relu, name=name)
    return x
def createDataSet():
    (picture_files, classes) = rd.retrieveLabeledData(datasetPath)
    (path, data, label) = zip(*picture_files)

    data = [x for x in data]
    classes = [x for x in classes]
    path = [x for x in path]

    lookup = dict(zip(classes, range(len(classes))))
    print(lookup)
    label = [lookup[x] for x in label]


    print("total numbers of images: ", len(data))
    data_for_test = 147
    data_for_validation = 140
    training_images = [x for x, y in zip(data, range(
        len(data))) if y % data_for_test != 0 and y % data_for_validation != 0]
    training_labels = [x for x, y in zip(label, range(
        len(label))) if y % data_for_test != 0 and y % data_for_validation != 0]
    training_paths = [x for x, y in zip(path, range(
        len(path))) if y % data_for_test != 0 and y % data_for_validation != 0]

    print("training images: ", len(training_images))
    print("training labels: ", len(training_labels))
    print("training paths:", len(training_paths))

    testing_images = [x for x, y in zip(data, range(
        len(data))) if y % data_for_test == 0 and y % data_for_validation != 0]
    testing_labels = [x for x, y in zip(label, range(
        len(label))) if y % data_for_test == 0 and y % data_for_validation != 0]
    testing_paths = [x for x, y in zip(path, range(
        len(path))) if y % data_for_test == 0 and y % data_for_validation != 0]

    print("testing images:", len(testing_images))
    print("testing labels:", len(testing_labels))
    print("testing paths:", len(testing_paths))

    validation_images = [x for x, y in zip(data, range(
        len(data))) if y % data_for_test != 0 and y % data_for_validation == 0]
    validation_labels = [x for x, y in zip(label, range(
        len(label))) if y % data_for_test != 0 and y % data_for_validation == 0]
    validation_paths = [x for x, y in zip(path, range(
        len(path))) if y % data_for_test != 0 and y % data_for_validation == 0]


    print("validation images:", len(validation_images))
    print("validation labels:", len(validation_labels))
    print("validation paths:", len(validation_paths))

    batch_size = 128
    height, width, _ = ds.getSize()

    n_outputs = len(classes)

    tf.reset_default_graph()

    training_dataset = tf.data.Dataset.from_tensor_slices(
        (training_paths, training_images, training_labels))
    training_dataset = training_dataset.shuffle(len(training_images))
    training_dataset = training_dataset.apply(tf.contrib.data.map_and_batch(
        map_func=rd.decodeAndResize, batch_size=tf.placeholder_with_default(
            tf.constant(batch_size, dtype=tf.int64), ()),
        num_parallel_batches=1))
    training_dataset = training_dataset.prefetch(batch_size)


    testing_dataset = tf.data.Dataset.from_tensor_slices(
        (testing_paths, testing_images, testing_labels))
    testing_dataset = testing_dataset.apply(tf.contrib.data.map_and_batch(
        map_func=rd.decodeAndResize, batch_size=tf.placeholder_with_default(
            tf.constant(len(testing_images), dtype=tf.int64), ()),
        num_parallel_batches=1))
    testing_dataset = testing_dataset.prefetch(len(testing_images))

    validation_dataset = tf.data.Dataset.from_tensor_slices(
        (validation_paths, validation_images, validation_labels))
    validation_dataset = validation_dataset.apply(tf.contrib.data.map_and_batch(
        map_func=rd.decodeAndResize, batch_size=tf.placeholder_with_default(
            tf.constant(len(validation_images), dtype=tf.int64), ()),
        num_parallel_batches=1))
    validation_dataset = validation_dataset.prefetch(len(validation_images))

    return training_dataset, testing_dataset, validation_dataset, n_outputs

training_dataset, testing_dataset, validation_dataset, n_outputs= createDataSet()

iterator = tf.data.Iterator.from_structure(training_dataset.output_types,
                                           training_dataset.output_shapes)

training_init_op = iterator.make_initializer(training_dataset)
testing_init_op = iterator.make_initializer(testing_dataset)
validation_init_op = iterator.make_initializer(validation_dataset)

next_x, next_y = iterator.get_next()
with tf.device("/gpu:0"):
    # X = tf.placeholder(next_x, shape=[channels, height, width, None], name="X")
    # dropout_rate= 0.3

    # training = tf.placeholder_with_default(False, shape=(), name="training")
    # X_drop = tf.layers.dropout(next_x, dropout_rate, training = training)

    # y = tf.placeholder(tf.int32, shape = [None], name="y")
    conv1 = conv2d(next_x, 64, 3, "conv1")

    pool2 = tf.nn.max_pool(conv1,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 1, 1, 1],
                           padding="VALID")

    conv2 = conv2d(pool2, 128, 3, "conv2", 2)

    pool2 = tf.nn.max_pool(conv2,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 2, 2, 1],
                           padding="VALID")

    conv3 = conv2d(pool2, 64, 4, "conv3", 3)

    pool3 = tf.nn.max_pool(conv3,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 1, 1, 1],
                           padding="VALID")
    # print("\n"*5)
    # print(pool3)
    pool3_flat = tf.reshape(pool3, shape=[-1, 8*8*64])

    fullyconn1 = tf.layers.dense(
        pool3_flat, 64, activation=tf.nn.relu, name="fc1")
    fullyconn2 = tf.layers.dense(
        fullyconn1, 64, activation=tf.nn.relu, name="fc2")
    # fullyconn3 = tf.layers.dense(fullyconn2, 256, activation=tf.nn.relu, name = "fc3")
    # fullyconn4 = tf.layers.dense(fullyconn3, 500, activation=tf.nn.relu, name = "fc4")

    logits = tf.layers.dense(fullyconn2, n_outputs, name="output")

    xentropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=logits, labels=next_y)

    loss = tf.reduce_mean(xentropy)
    optimizer = tf.train.AdamOptimizer()
    # optimizer =  tf.train.GradientDescentOptimizer(0.5)
    training_op = optimizer.minimize(loss)

    correct = tf.nn.in_top_k(logits, next_y, 1)
    accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))
    tf.summary.scalar('accuracy', accuracy)
    merged = tf.summary.merge_all()
    test_writer = tf.summary.FileWriter('./accuracy')
    init = tf.global_variables_initializer()
saver = tf.train.Saver()

hm_epochs = 10
config = tf.ConfigProto(allow_soft_placement=True)
config.gpu_options.allow_growth = True


startTime = datetime.now()
with tf.Session(config=config) as sess:
    logPath = "."
    tbWriter = tf.summary.FileWriter(logPath, sess.graph)

    sess.run(tf.global_variables_initializer())
    # saver.restore(sess, "./biggerDataSet.ckpt")
    # print("Model restored.")
    print("Training has started")
    for epoch in range(hm_epochs):
        epoch_start = datetime.now()
        epoch_loss = 0
        sess.run(training_init_op)
        while True:
            try:
                sess.run(training_op)
            except tf.errors.OutOfRangeError:
                break


        sess.run(validation_init_op)
        try:
            summary, acc_validation = sess.run([merged, accuracy])
            test_writer.add_summary(summary, epoch)
            print("Epoch {0}: Validation accuracy {1}, Time taken {2}".format(
                epoch, acc_validation, datetime.now() - epoch_start))
        except tf.errors.OutOfRangeError:
            break
        # print("Epoch {0}: Train accuracy {1}, Test accuracy: {2}".format(epoch, acc_train, acc_test))
    sess.run(testing_init_op)
    try:
        acc_test = accuracy.eval()
        print("Accuracy: {0}".format(acc_test))
    except tf.errors.OutOfRangeError:
        print("Exception")
    
    save_path = saver.save(sess, "./streets.ckpt")

print("Time taken:", datetime.now() - startTime)
