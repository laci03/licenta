import os

def getSize():
    height = 100
    width = 100
    channels = 3
    return height, width, channels

def getDatasetPath():
    path = "../../../Licenta_date/data/processed/resized_100_100/"
    return path

def getNrOfOutputs():
    return len(getClasses())

def getClasses():
    datasetPath = getDatasetPath()
    classes = os.listdir(datasetPath)
    return classes