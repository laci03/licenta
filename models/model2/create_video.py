import tensorflow as tf
import numpy as np
import sys
import retrieveData as rd
import cv2
import os
import details as ds
from datetime import datetime
# Add ops to save and restore all the variables.
height, width, channels = ds.getSize()
n_outputs = ds.getNrOfOutputs()


def getImage(path):
    image_string = tf.read_file(path)
    image_decoded = tf.image.decode_jpeg(image_string)
    image_resized = tf.image.resize_images(image_decoded, [height, width])
    image_resized.set_shape([height, width, channels])
    return tf.expand_dims(image_resized, 0)


tf.reset_default_graph()


def conv2d(x, filters, ksize, name, strides=1):
    x = tf.layers.conv2d(x, filters=filters, kernel_size=ksize,
                         strides=strides, padding="SAME",
                         activation=tf.nn.relu, name=name)
    return x


with tf.device("/gpu:0"):
    X = tf.placeholder(tf.float32, shape=[
                       1, height, width, channels], name="X")

    conv1 = conv2d(X, 64, 3, "conv1")

    pool2 = tf.nn.max_pool(conv1,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 1, 1, 1],
                           padding="VALID")

    conv2 = conv2d(pool2, 128, 3, "conv2", 2)

    pool2 = tf.nn.max_pool(conv2,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 2, 2, 1],
                           padding="VALID")

    conv3 = conv2d(pool2, 64, 4, "conv3", 3)

    pool3 = tf.nn.max_pool(conv3,
                           ksize=[1, 2, 2, 1],
                           strides=[1, 1, 1, 1],
                           padding="VALID")

    pool3_flat = tf.reshape(pool3, shape=[-1, 64*8*8])

    fullyconn1 = tf.layers.dense(
        pool3_flat, 64, activation=tf.nn.relu, name="fc1")
    fullyconn2 = tf.layers.dense(
        fullyconn1, 64, activation=tf.nn.relu, name="fc2")

    logits = tf.layers.dense(fullyconn2, n_outputs, name="output")

    prediction = tf.nn.softmax(logits)
    init = tf.global_variables_initializer()

def main(name, step, fps, path):
    startTime = datetime.now()
    createVideo(name, step, fps, path)
    print("Time taken:", datetime.now() - startTime)


def createVideo(name, step, fps, folderPath):
    (picture_files, classes) = rd.retrieveLabeledData(folderPath)
    (allPaths, data, _) = zip(*picture_files)
    config = tf.ConfigProto(allow_soft_placement=True)
    saver = tf.train.Saver()
    data = [x for x in data]
    classes = [x for x in classes]
    allPaths = [x for x in allPaths]

    img1 = cv2.imread(allPaths[0]+data[0])
    videoHeight, videoWidth, layers = img1.shape
    # print(videoHeight, videoWidth)
    fourcc = cv2.VideoWriter_fourcc(*'DIVX')
    video = cv2.VideoWriter(name, fourcc, fps, (videoWidth, videoHeight))
    counter = 0
    last_percent = 0
    current_percent = 0
    length = len(data)
    font = cv2.FONT_HERSHEY_SIMPLEX
    bottom_left_corner_of_text = (400, 100)
    fontScale = 5
    fontColor = (0, 0, 255)
    lineType = 4

    with tf.Session(config=config) as sess:
        # Restore variables from disk.
        sess.run(init)
        saver.restore(sess, "./streets.ckpt")
        print("Model restored.")
        for path, data in zip(allPaths, data):
            if(counter % step == 0 ):
                current_percent = counter / length
                if(last_percent*100 + 1 < current_percent*100):
                    os.system('cls' if os.name == 'nt' else 'clear')
                    print("{0}%".format(int(current_percent*100)))
                    last_percent = current_percent
                img = cv2.imread(path+data)
                probabilities = sess.run(prediction, feed_dict={
                                        X: getImage(path+data).eval()})[0]
                _, max_idx = max([(v, i) for i, v in enumerate(probabilities)])

                cv2.putText(img, classes[max_idx],
                            bottom_left_corner_of_text,
                            font,
                            fontScale,
                            fontColor,
                            lineType)
                video.write(img)

                cv2.destroyAllWindows()
            counter = counter + 1
        video.release()

    print("Done")


if __name__ == '__main__':
    step = 30
    fps=30.0
    path="../../../Licenta_date/data/interim/original/"
    if len(sys.argv)>= 3:
        step = int(sys.argv[2])
    if len(sys.argv)>= 4:
        fps = int(sys.argv[3])
    if len(sys.argv)>= 5:
        path = sys.argv[4]
    
    
    main(sys.argv[1], step, fps, path)
