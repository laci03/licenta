import numpy as np
import cv2
import os
import argparse


def resize_image_from_path(img_path, height, width, name='unknown.jpg', verbose=False):
    img = cv2.imread(img_path)
    new_img = cv2.resize(img, (width, height), interpolation=cv2.INTER_CUBIC)

    if verbose:
        cv2.imwrite(name, new_img)
    return new_img


def resize_img(img, height, width, name='unknown.jpg'):
    new_img = cv2.resize(img, (width, height), interpolation=cv2.INTER_CUBIC)
    cv2.imwrite(name, new_img)
    return new_img


def resize_new_files(path='F:/Laci/Licenta/Licenta_date/interim/Aiud/etichetate/', resize_path='', height=94,
                     width=168):
    streets = os.listdir(path)
    if resize_path:
        resized_path = os.path.join(path, '../../../processed/resized_aiud_{0}_{1}'.format(height, width))
    else:
        resized_path = resize_path

    if not os.path.isdir(resized_path):
        os.mkdir(resized_path)

    for street in streets:
        print('Start resizing for:', street)

        path_to_street = os.path.join(resized_path, '{0}'.format(street))

        if not os.path.isdir(path_to_street):
            os.mkdir(path_to_street)

        resized_files = os.listdir(path_to_street)
        files_to_resize = os.listdir(path + '{0}/'.format(street))
        files_to_resize = list(set(files_to_resize) - set(resized_files))

        counter = 0
        last_percent = 0
        length = len(files_to_resize)

        for name in files_to_resize:
            current_percent = counter / length

            if last_percent * 100 + 1 < current_percent * 100:
                print('{0}%'.format(int(current_percent * 100)), end='\r')
                last_percent = current_percent
            counter += 1
            if name[-4:] == '.jpg':
                img_path = os.path.join(path, '{0}/{1}'.format(street, name))
                save_to = os.path.join(path_to_street, name)
                resize_image_from_path(img_path, height, width, save_to, verbose=True)
                remove_middle_from_image(img_path, name, path_to_street)
                averaging_filter(img_path, height, width, path_to_street, name)
                blur(img_path, height, width, path_to_street, name)
                gaussian_blur(img_path, height, width, path_to_street, name)
                image_to_gray_scale(path_to_street, name)
    print('Done Resizing all')


def resize_all(path='F:/Laci/Licenta/Licenta_date/interim/Aiud/etichetate/', resize_path='', height=94, width=168):
    streets = os.listdir(path)
    if resize_path:
        resized_path = resize_path
    else:
        resized_path = path + '../../../processed/' + 'resized_aiud_{0}_{1}/'.format(height, width)

    if not os.path.isdir(resized_path):
        os.mkdir(resized_path)

    for street in streets:
        print('Start resizing for:', street)
        path_to_street = os.path.join(resized_path, '{0}'.format(street))

        if not os.path.isdir(path_to_street):
            os.mkdir(path_to_street)

        files_to_resize = os.listdir(os.path.join(path, '{0}/'.format(street)))

        counter = 0
        last_percent = 0
        length = len(files_to_resize)
        for name in files_to_resize:
            current_percent = counter / length
            if last_percent * 100 + 1 < current_percent * 100:
                print('{0}%'.format(int(current_percent * 100)), end='\r')
                last_percent = current_percent
            counter += 1

            if name[-4:] == '.jpg':
                img_path = os.path.join(path, '{0}/{1}'.format(street, name))
                save_to = os.path.join(path_to_street, name)
                resize_image_from_path(img_path, height, width, save_to, verbose=True)
                # remove_middle_from_image(img_path, name, path_to_street)
                # averaging_filter(img_path, height, width, path_to_street, name)
                # blur(img_path, height, width, path_to_street, name)
                # gaussian_blur(img_path, height, width, path_to_street, name)
                # image_to_gray_scale(path_to_street, name)
    print('Done Resizing all')


def resize_all_segments(path='F:/Laci/Licenta/Licenta_date_2_CNN/interim/smailo_Laci/labeled/', resize_path='',
                        height=94, width=168):
    streets = os.listdir(path)

    if resize_path == '':
        resized_path = path + '../../../processed/' + 'resized_aiud_{0}_{1}/'.format(height, width)
    else:
        resized_path = resize_path

    if not os.path.isdir(resized_path):
        os.mkdir(resized_path)

    for street in streets:
        print('Start resizing for:', street)
        path_to_street = resized_path + '{0}/'.format(street)
        if not os.path.isdir(path_to_street):
            os.mkdir(path_to_street)
        segments = os.listdir(path + street)
        print(segments)

        for segment in segments:
            print(segment)
            path_to_segment = path_to_street + segment + '/'

            if not os.path.isdir(path_to_segment):
                os.mkdir(path_to_segment)

            files_to_resize = os.listdir(path + '{0}/{1}/'.format(street, segment))
            counter = 0
            last_percent = 0
            current_percent = 0
            length = len(files_to_resize)

            for name in files_to_resize:
                current_percent = counter / length
                if last_percent * 100 + 1 < current_percent * 100:
                    print('{0}%'.format(int(current_percent * 100)), end='\r')
                    last_percent = current_percent
                counter += 1

                if name[-4:] == '.jpg':
                    img_path = path + '{0}/{1}/{2}'.format(street, segment, name)
                    save_to = path_to_segment + name
                    resize_image_from_path(img_path, height, width, save_to, verbose=True)
                    # remove_middle_from_image(img_path, name, path_to_segment)
                    # averaging_filter(img_path, height, width, path_to_segment, name)
                    # blur(img_path, height, width, path_to_segment, name)
                    # gaussian_blur(img_path, height, width, path_to_segment, name)
                    # image_to_gray_scale(path_to_street, name)
    print('Done Resizing all')


def resize_all_new_segments(path='F:/Laci/Licenta/Licenta_date_2_CNN/interim/smailo_Laci/labeled/', resize_path='',
                            height=94, width=168):
    streets = os.listdir(path)
    if resize_path != '':
        resized_path = resize_path
    else:
        resized_path = path + '../../../processed/' + 'resized_aiud_{0}_{1}/'.format(height, width)

    if not os.path.isdir(resized_path):
        os.mkdir(resized_path)

    for street in streets:
        print('Start resizing for:', street)
        path_to_street = resized_path + '{0}/'.format(street)

        if not os.path.isdir(path_to_street):
            os.mkdir(path_to_street)

        segments = os.listdir(path + street)
        print(segments)

        for segment in segments:
            path_to_segment = path_to_street + segment + '/'
            if not os.path.isdir(path_to_segment):
                os.mkdir(path_to_segment)

            resized_files = os.listdir(path_to_segment)
            files_to_resize = os.listdir(path + '{0}/{1}/'.format(street, segment))
            files_to_resize = list(set(files_to_resize) - set(resized_files))

            counter = 0
            last_percent = 0
            current_percent = 0
            length = len(files_to_resize)

            for name in files_to_resize:
                current_percent = counter / length
                if last_percent * 100 + 1 < current_percent * 100:
                    print('{0}%'.format(int(current_percent * 100)), end='\r')
                    last_percent = current_percent
                counter += 1

                if name[-4:] == '.jpg':
                    img_path = path + '{0}/{1}/{2}'.format(street, segment, name)
                    save_to = path_to_segment + name
                    resize_image_from_path(img_path, height, width, save_to, verbose=True)
                    averaging_filter(img_path, height, width, path_to_segment, name)
                    blur(img_path, height, width, path_to_segment, name)
                    gaussian_blur(img_path, height, width, path_to_segment, name)
                    # image_to_gray_scale(path_to_street, name)
    print('Done Resizing all')


def rotate_all_images(path, direction=1):
    """
        Get as input a path and a direction
        and will rotate all the images from that folder
        1 - left
        -1 - right
    """
    files = os.listdir(path)

    # Retrieving picture names
    images = [name for name in files if name[-4:] == '.jpg']
    length = len(images)

    counter = 0
    last_percent = 0
    current_percent = 0

    print('Start rotating all images')
    for image in images:
        current_percent = counter / length
        if last_percent * 100 + 1 < current_percent * 100:
            print('{0}%'.format(int(current_percent * 100)), end='\r')
            last_percent = current_percent
        counter = counter + 1
        img = cv2.imread(path + image)
        rows, cols, _ = img.shape
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), direction * 90, 1)
        dst = cv2.warpAffine(img, M, (cols, rows))

        cv2.imwrite(path + image, dst)
    print('Done!')


def to_gray_scale_multiple_folders(path, prefix='gr_'):
    streets = os.listdir(path)

    for street in streets:
        print('Converting folder: {0} to gray scale'.format(street))
        for name in os.listdir(path + '{0}/'.format(street)):
            if name[-4:] == '.jpg':
                image_to_gray_scale(path + street + '/', name, prefix)
    print('Done converting all')


def to_rgb_multiple_folders(path):
    streets = os.listdir(path)
    for street in streets:
        print('Converting folder: {0} to RGB'.format(street))
        to_rgb(path + street + '/')
    print('Done converting all')


def to_gray_scale(path, prefix='gr_'):
    """Get as input a path
    and will convert all the images from that folder to gray scale"""

    files = os.listdir(path)

    # Retrieving picture names
    images = [name for name in files if name[-4:] == '.jpg']
    length = len(images)

    counter = 0
    last_percent = 0
    current_percent = 0

    print('Start converting to gray scale')
    for image in images:
        current_percent = counter / length
        if last_percent * 100 + 1 < current_percent * 100:
            print('{0}%'.format(int(current_percent * 100)), end='\r')
            last_percent = current_percent
        counter = counter + 1
        img = cv2.imread(path + image)
        gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(path + prefix + image, gray_image)
    print('Done!')


def image_to_gray_scale(path, name, prefix='gr_'):
    img = cv2.imread(path + name)
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imwrite(path + prefix + name, gray_image)


def image_to_red(path, name, prefix='red_'):
    img = cv2.imread(path + name)
    x, y, _ = img.shape
    for i in range(x):
        for j in range(y):
            img[i, j, 0] = 0
            img[i, j, 1] = 0
    cv2.imwrite(path + prefix + name, img)


def remove_middle_from_image(img_path, name, path_to_save):
    img = cv2.imread(img_path)
    x, y, _ = img.shape
    for i in range(x):
        for j in range(int(y / 3), 2 * int(y / 3)):
            img[i, j, 0] = 0
            img[i, j, 1] = 0
            img[i, j, 2] = 0
    cv2.imwrite(path_to_save + 'black_middle_' + name, img)


def image_to_blue(path, name, prefix='blue_'):
    img = cv2.imread(path + name)
    x, y, _ = img.shape
    for i in range(x):
        for j in range(y):
            img[i, j, 1] = 0
            img[i, j, 2] = 0
    cv2.imwrite(path + prefix + name, img)


def image_to_green(path, name, prefix='green_'):
    img = cv2.imread(path + name)
    x, y, _ = img.shape
    for i in range(x):
        for j in range(y):
            img[i, j, 0] = 0
            img[i, j, 2] = 0
    cv2.imwrite(path + prefix + name, img)


def to_rgb(path):
    """Get as input a path
    and will convert all the images from that folder to RGB"""
    files = os.listdir(path)

    # Retrieving picture names
    images = [name for name in files if name[-4:] == '.jpg']

    length = len(images)
    counter = 0
    last_percent = 0
    current_percent = 0

    print('Start converting to RGB')
    for image in images:
        current_percent = counter / length
        if last_percent * 100 + 1 < current_percent * 100:
            print('{0}%'.format(int(current_percent * 100)), end='\r')
            last_percent = current_percent
        counter = counter + 1
        image_to_red(path, image)
        image_to_blue(path, image)
        image_to_green(path, image)

    print('Done!')


def averaging_filter(img_path, height, width, path_to_street, name):
    img = cv2.imread(img_path)

    kernel_3_3 = np.ones((3, 3), np.float32) / 25
    kernel_4_4 = np.ones((4, 4), np.float32) / 25
    kernel_5_5 = np.ones((5, 5), np.float32) / 25
    kernel_6_6 = np.ones((6, 6), np.float32) / 25
    kernel_7_7 = np.ones((7, 7), np.float32) / 25
    dst_3_3 = cv2.filter2D(img, -1, kernel_3_3)
    dst_4_4 = cv2.filter2D(img, -1, kernel_4_4)
    dst_5_5 = cv2.filter2D(img, -1, kernel_5_5)
    dst_6_6 = cv2.filter2D(img, -1, kernel_6_6)
    dst_7_7 = cv2.filter2D(img, -1, kernel_7_7)

    resize_img(dst_3_3, height, width, path_to_street + 'avg_3_3_' + name)
    resize_img(dst_4_4, height, width, path_to_street + 'avg_4_4_' + name)
    resize_img(dst_5_5, height, width, path_to_street + 'avg_5_5_' + name)
    resize_img(dst_6_6, height, width, path_to_street + 'avg_6_6_' + name)
    resize_img(dst_7_7, height, width, path_to_street + 'avg_7_7_' + name)


def blur(img_path, height, width, path_to_street, name):
    img = cv2.imread(img_path)
    blur7_7 = cv2.blur(img, (7, 7))
    resize_img(blur7_7, height, width, path_to_street + 'blur_7_7' + name)


def move_from_to(source_path, destination):
    streets = os.listdir(source_path)

    for street in streets:
        print('Start moving for:', street)
        destination_street = destination + '{0}/'.format(street)

        if not os.path.isdir(destination_street):
            os.mkdir(destination_street)

        segments = os.listdir(source_path + '{0}/'.format(street))
        for segment in segments:
            files = os.listdir(source_path + '{0}/{1}/'.format(street, segment))
            for name in files:
                img_path = source_path + '{0}/{1}/{2}'.format(street, segment, name)
                save_to = destination_street + segment + '/' + name
                os.rename(img_path, save_to)
    print('Done moving all')


def gaussian_blur(img_path, height, width, path_to_street, name):
    img = cv2.imread(img_path)
    blur_7_7 = cv2.gaussian_blur(img, (7, 7), 0)
    resize_img(blur_7_7, height, width, path_to_street + 'gaussian_blur_7_7' + name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_type', default='0')
    parser.add_argument('--path', default='g:/Laci/Licenta/Licenta_date/interim/Aiud/etichetate/')
    parser.add_argument('--resize_path', default='')
    parser.add_argument('--height', default='94')
    parser.add_argument('--width', default='168')

    args = parser.parse_args()

    if int(args.model_type) == 0:
        resize_all(path=args.path, resize_path=args.resize_path, height=args.height, width=args.width)
    else:
        resize_all_segments(path=args.path, resize_path=args.resize_path, height=args.height, width=args.width)
