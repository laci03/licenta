import numpy as np
import cv2
import sys
import os
import argparse


def all_videos_to_frames(path, output_path, start_from=0):
    files = os.listdir(path)
    counter = start_from
    i = 1
    for file in files:
        print('{0} ({1}/{2})'.format(file, i, len(files)))
        i += 1
        counter = video_to_frames(path + file, output_path, counter)


def video_to_frames(video_path, output_path, start_from):
    video = cv2.VideoCapture(video_path)
    fps = video.get(cv2.CAP_PROP_FPS)
    print('fps={0}'.format(fps))

    length = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    counter = 0
    last_percent = 0
    current_percent = 0

    while True:
        current_percent = counter / length
        if last_percent * 100 + 1 < current_percent * 100:
            print('{0}%'.format(int(current_percent * 100)), end='\r')
            last_percent = current_percent

        ret, frame = video.read()
        if ret is False:
            break
        cv2.imwrite(
            output_path + '/frame-{0}'.format(counter + start_from) + '.jpg', frame)

        counter += 1
    print('Done!')
    video.release()
    return counter + start_from


def video_to_frames_by_timmer(video_path, output_path, timer_path, start_from):
    video = cv2.VideoCapture(video_path)
    fps = video.get(cv2.CAP_PROP_FPS)
    print('fps={0}'.format(fps))

    length = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    counter = 0
    last_percent = 0
    current_percent = 0

    file = open(timer_path, 'r')
    groups = file.read().split('\n')
    groups = [int(x) for x in groups]

    sum = 0
    i = 0

    if not os.path.isdir(output_path + '{0}'.format(i)):
        os.mkdir(output_path + '{0}'.format(i))

    while True:
        current_percent = counter / length
        if last_percent * 100 + 1 < current_percent * 100:
            print('{0}%'.format(int(current_percent * 100)), end='\r')
            last_percent = current_percent

        ret, frame = video.read()
        if ret is False:
            break

        cv2.imwrite(
            output_path + '{0}/frame-{1}'.format(i, counter + start_from) + '.jpg', frame)

        counter += 1

        if counter > sum + groups[i] and len(groups) != i + 1:
            sum += groups[i]
            i += 1

            if not os.path.isdir(output_path + '{0}'.format(i)):
                os.mkdir(output_path + '{0}'.format(i))

    print('Done!')
    video.release()
    return counter + start_from


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--video_path', help='Path to the video')
    parser.add_argument('--output_path', help='Path to the folder in which we will save the frames')
    parser.add_argument('--start_from', default='0')

    args = parser.parse_args()

    video_to_frames(args.video_path, args.output_path, int(args.start_from))
    # all_videos_to_frames(args.video_path, args.output_path, int(args.start_from))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--video_path', help='Path to the video')
    parser.add_argument('--output_path', help='Path to the folder in which we will save the frames')
    parser.add_argument('--timer_path', help='Path to the timer txt file')
    parser.add_argument('--start_from', default='0')

    args = parser.parse_args()

    video_to_frames_by_timmer(args.video_path, args.output_path, args.timer_path, int(args.start_from))
