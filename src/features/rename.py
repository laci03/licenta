import os
import argparse


def rename_all(path, name):
    """
        Gets as input 2 parameters: a path to a folder and a new name.
        All pictures from that folder will be renamed with the new name starting from 0.
        example: renameAll('d:/Laci/', 'test') - will rename all pictures from folder d:/Laci/ to test0, test1 and so on.
    """
    files = os.listdir(path)
    i = 0
    for file in files:
        if '.jpg' in file:
            os.rename(os.path.join(path, file), os.path.join(path, ''.join([name, '{0}.jpg'.format(i)])))
        i += 1


def add_prefix(path, prefix='uk_'):
    """
        Gets as input 2 parameters: a path to a folder and a prefix.
        All pictures from that folder will be renamed to prefix + oldname.
        example: add_prefix('d:/Laci/', 'test_') - will rename all pictures from folder d:/Laci/ to test_oldName and so on.
    """
    files = os.listdir(path)
    counter = 0
    last_percent = 0
    length = len(files)
    for file in files:
        current_percent = counter / length
        if last_percent * 100 + 1 < current_percent * 100:
            print('{0}%'.format(int(current_percent * 100)), end='\r')
            last_percent = current_percent
        os.rename(os.path.join(path, file), os.path.join(path, ''.join([prefix, file])))
        counter += 1


def add_prefix_segment(path, prefix='uk'):
    streets = os.listdir(path)
    for street in streets:
        path_to_street = os.path.join(path, street)
        segments = os.listdir(path_to_street)
        for segment in segments:
            add_prefix(os.path.join(path_to_street, segment), prefix)


def rename_all_by_folder(path):
    """
        Gets as input 1 parameter: a path to a folder which contains multiple folders.
        All pictures from that folders will be renamed to the folder name in which they are.
        example: renameAllByFolder('d:/Laci/') - contains folder test, so all pictures from that folder
        will be renamed to test1.jpg, test2.jpg, and so on.
    """
    folders = os.listdir(path)
    for folder in folders:
        rename_all(os.path.join(path, folder), folder)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, help='the path of the folder where the images are.')
    parser.add_argument('--new_name', type=str, default='', help='The new name to which the images will be renamed')

    args = vars(parser.parse_args())

    rename_all(args['path'], args['new_name'])
