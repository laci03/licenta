import os, argparse

import tensorflow as tf
import details as ds
# The original freeze_graph function
# from tensorflow.python.tools.freeze_graph import freeze_graph 

dir = os.path.dirname(os.path.realpath(__file__))

def freeze_graph(model_dir, output_node_names):
    """Extract the sub graph defined by the output nodes and convert
    all its variables into constant 
    Args:
        model_dir: the root folder containing the checkpoint state file
        output_node_names: a string, containing all the output node's names, 
                            comma separated
    """
    if not tf.gfile.Exists(model_dir):
        raise AssertionError(
            'Export directory doesn't exists. Please specify an export '
            'directory: %s' % model_dir)

    # We retrieve our checkpoint fullpath
    checkpoint = tf.train.get_checkpoint_state(model_dir)
    input_checkpoint = checkpoint.model_checkpoint_path
    
    # We precise the file fullname of our freezed graph
    absolute_model_dir = '/'.join(input_checkpoint.split('/')[:-1])
    output_graph = absolute_model_dir + '/frozen_model.pb'

    # We clear devices to allow TensorFlow to control on which device it will load operations
    clear_devices = True

    # We start a session using a temporary fresh Graph
    with tf.Session(graph=tf.Graph()) as sess:
        # We import the meta graph in the current default Graph
        saver = tf.train.import_meta_graph(input_checkpoint + '.meta', clear_devices=clear_devices)

        # We restore the weights
        saver.restore(sess, input_checkpoint)

        # We use a built-in TF helper to export variables to constants
        output_graph_def = tf.graph_util.convert_variables_to_constants(
            sess, # The session is used to retrieve the weights
            tf.get_default_graph().as_graph_def() , # The graph_def is used to retrieve the nodes 
            output_node_names.split(',') # The output node names are used to select the usefull nodes
        ) 

        # Finally we serialize and dump the output graph to the filesystem
        with tf.gfile.GFile(output_graph, 'wb') as f:
            f.write(output_graph_def.SerializeToString())
        print('%d ops in the final graph.' % len(output_graph_def.node))

    return output_graph_def

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_dir', type=str, default='', help='Model folder to export')
    parser.add_argument('--output_node_names', type=str, default='', help='The name of the output nodes, comma separated.')
    args = parser.parse_args()
    # output_node_names = ['X', 'training/input', 'training', 'dropout/cond/Switch', 'dropout/cond/switch_t', 'dropout/cond/switch_f', 'dropout/cond/pred_id', 'dropout/cond/dropout/keep_prob', 'dropout/cond/dropout/Shape', 'dropout/cond/dropout/random_uniform/min', 'dropout/cond/dropout/random_uniform/max', 'dropout/cond/dropout/random_uniform/RandomUniform', 'dropout/cond/dropout/random_uniform/sub', 'dropout/cond/dropout/random_uniform/mul', 'dropout/cond/dropout/random_uniform', 'dropout/cond/dropout/add', 'dropout/cond/dropout/Floor', 'dropout/cond/dropout/div', 'dropout/cond/dropout/div/Switch', 'dropout/cond/dropout/mul', 'dropout/cond/Identity', 'dropout/cond/Identity/Switch', 'dropout/cond/Merge', 'conv1/kernel/Initializer/random_uniform/shape', 'conv1/kernel/Initializer/random_uniform/min', 'conv1/kernel/Initializer/random_uniform/max', 'conv1/kernel/Initializer/random_uniform/RandomUniform', 'conv1/kernel/Initializer/random_uniform/sub', 'conv1/kernel/Initializer/random_uniform/mul', 'conv1/kernel/Initializer/random_uniform', 'conv1/kernel', 'conv1/kernel/Assign', 'conv1/kernel/read', 'conv1/bias/Initializer/zeros/shape_as_tensor', 'conv1/bias/Initializer/zeros/Const', 'conv1/bias/Initializer/zeros', 'conv1/bias', 'conv1/bias/Assign', 'conv1/bias/read', 'conv1/dilation_rate', 'conv1/Conv2D', 'conv1/BiasAdd', 'conv1/Relu', 'dropout_1/cond/Switch', 'dropout_1/cond/switch_t', 'dropout_1/cond/switch_f', 'dropout_1/cond/pred_id', 'dropout_1/cond/dropout/keep_prob', 'dropout_1/cond/dropout/Shape', 'dropout_1/cond/dropout/random_uniform/min', 'dropout_1/cond/dropout/random_uniform/max', 'dropout_1/cond/dropout/random_uniform/RandomUniform', 'dropout_1/cond/dropout/random_uniform/sub', 'dropout_1/cond/dropout/random_uniform/mul', 'dropout_1/cond/dropout/random_uniform', 'dropout_1/cond/dropout/add', 'dropout_1/cond/dropout/Floor', 'dropout_1/cond/dropout/div', 'dropout_1/cond/dropout/div/Switch', 'dropout_1/cond/dropout/mul', 'dropout_1/cond/Identity', 'dropout_1/cond/Identity/Switch', 'dropout_1/cond/Merge', 'pool1', 'conv2/kernel/Initializer/random_uniform/shape', 'conv2/kernel/Initializer/random_uniform/min', 'conv2/kernel/Initializer/random_uniform/max', 'conv2/kernel/Initializer/random_uniform/RandomUniform', 'conv2/kernel/Initializer/random_uniform/sub', 'conv2/kernel/Initializer/random_uniform/mul', 'conv2/kernel/Initializer/random_uniform', 'conv2/kernel', 'conv2/kernel/Assign', 'conv2/kernel/read', 'conv2/bias/Initializer/zeros/shape_as_tensor', 'conv2/bias/Initializer/zeros/Const', 'conv2/bias/Initializer/zeros', 'conv2/bias', 'conv2/bias/Assign', 'conv2/bias/read', 'conv2/dilation_rate', 'conv2/Conv2D', 'conv2/BiasAdd', 'conv2/Relu', 'dropout_2/cond/Switch', 'dropout_2/cond/switch_t', 'dropout_2/cond/switch_f', 'dropout_2/cond/pred_id', 'dropout_2/cond/dropout/keep_prob', 'dropout_2/cond/dropout/Shape', 'dropout_2/cond/dropout/random_uniform/min', 'dropout_2/cond/dropout/random_uniform/max', 'dropout_2/cond/dropout/random_uniform/RandomUniform', 'dropout_2/cond/dropout/random_uniform/sub', 'dropout_2/cond/dropout/random_uniform/mul', 'dropout_2/cond/dropout/random_uniform', 'dropout_2/cond/dropout/add', 'dropout_2/cond/dropout/Floor', 'dropout_2/cond/dropout/div', 'dropout_2/cond/dropout/div/Switch', 'dropout_2/cond/dropout/mul', 'dropout_2/cond/Identity', 'dropout_2/cond/Identity/Switch', 'dropout_2/cond/Merge', 'pool2', 'conv3/kernel/Initializer/random_uniform/shape', 'conv3/kernel/Initializer/random_uniform/min', 'conv3/kernel/Initializer/random_uniform/max', 'conv3/kernel/Initializer/random_uniform/RandomUniform', 'conv3/kernel/Initializer/random_uniform/sub', 'conv3/kernel/Initializer/random_uniform/mul', 'conv3/kernel/Initializer/random_uniform', 'conv3/kernel', 'conv3/kernel/Assign', 'conv3/kernel/read', 'conv3/bias/Initializer/zeros/shape_as_tensor', 'conv3/bias/Initializer/zeros/Const', 'conv3/bias/Initializer/zeros', 'conv3/bias', 'conv3/bias/Assign', 'conv3/bias/read', 'conv3/dilation_rate', 'conv3/Conv2D', 'conv3/BiasAdd', 'conv3/Relu', 'dropout_3/cond/Switch', 'dropout_3/cond/switch_t', 'dropout_3/cond/switch_f', 'dropout_3/cond/pred_id', 'dropout_3/cond/dropout/keep_prob', 'dropout_3/cond/dropout/Shape', 'dropout_3/cond/dropout/random_uniform/min', 'dropout_3/cond/dropout/random_uniform/max', 'dropout_3/cond/dropout/random_uniform/RandomUniform', 'dropout_3/cond/dropout/random_uniform/sub', 'dropout_3/cond/dropout/random_uniform/mul', 'dropout_3/cond/dropout/random_uniform', 'dropout_3/cond/dropout/add', 'dropout_3/cond/dropout/Floor', 'dropout_3/cond/dropout/div', 'dropout_3/cond/dropout/div/Switch', 'dropout_3/cond/dropout/mul', 'dropout_3/cond/Identity', 'dropout_3/cond/Identity/Switch', 'dropout_3/cond/Merge', 'pool3', 'Reshape/shape', 'Reshape', 'fc1/kernel/Initializer/random_uniform/shape', 'fc1/kernel/Initializer/random_uniform/min', 'fc1/kernel/Initializer/random_uniform/max', 'fc1/kernel/Initializer/random_uniform/RandomUniform', 'fc1/kernel/Initializer/random_uniform/sub', 'fc1/kernel/Initializer/random_uniform/mul', 'fc1/kernel/Initializer/random_uniform', 'fc1/kernel', 'fc1/kernel/Assign', 'fc1/kernel/read', 'fc1/bias/Initializer/zeros/shape_as_tensor', 'fc1/bias/Initializer/zeros/Const', 'fc1/bias/Initializer/zeros', 'fc1/bias', 'fc1/bias/Assign', 'fc1/bias/read', 'fc1/MatMul', 'fc1/BiasAdd', 'fc1/Relu', 'dropout_4/cond/Switch', 'dropout_4/cond/switch_t', 'dropout_4/cond/switch_f', 'dropout_4/cond/pred_id', 'dropout_4/cond/dropout/keep_prob', 'dropout_4/cond/dropout/Shape', 'dropout_4/cond/dropout/random_uniform/min', 'dropout_4/cond/dropout/random_uniform/max', 'dropout_4/cond/dropout/random_uniform/RandomUniform', 'dropout_4/cond/dropout/random_uniform/sub', 'dropout_4/cond/dropout/random_uniform/mul', 'dropout_4/cond/dropout/random_uniform', 'dropout_4/cond/dropout/add', 'dropout_4/cond/dropout/Floor', 'dropout_4/cond/dropout/div', 'dropout_4/cond/dropout/div/Switch', 'dropout_4/cond/dropout/mul', 'dropout_4/cond/Identity', 'dropout_4/cond/Identity/Switch', 'dropout_4/cond/Merge', 'fc2/kernel/Initializer/random_uniform/shape', 'fc2/kernel/Initializer/random_uniform/min', 'fc2/kernel/Initializer/random_uniform/max', 'fc2/kernel/Initializer/random_uniform/RandomUniform', 'fc2/kernel/Initializer/random_uniform/sub', 'fc2/kernel/Initializer/random_uniform/mul', 'fc2/kernel/Initializer/random_uniform', 'fc2/kernel', 'fc2/kernel/Assign', 'fc2/kernel/read', 'fc2/bias/Initializer/zeros/shape_as_tensor', 'fc2/bias/Initializer/zeros/Const', 'fc2/bias/Initializer/zeros', 'fc2/bias', 'fc2/bias/Assign', 'fc2/bias/read', 'fc2/MatMul', 'fc2/BiasAdd', 'fc2/Relu', 'dropout_5/cond/Switch', 'dropout_5/cond/switch_t', 'dropout_5/cond/switch_f', 'dropout_5/cond/pred_id', 'dropout_5/cond/dropout/keep_prob', 'dropout_5/cond/dropout/Shape', 'dropout_5/cond/dropout/random_uniform/min', 'dropout_5/cond/dropout/random_uniform/max', 'dropout_5/cond/dropout/random_uniform/RandomUniform', 'dropout_5/cond/dropout/random_uniform/sub', 'dropout_5/cond/dropout/random_uniform/mul', 'dropout_5/cond/dropout/random_uniform', 'dropout_5/cond/dropout/add', 'dropout_5/cond/dropout/Floor', 'dropout_5/cond/dropout/div', 'dropout_5/cond/dropout/div/Switch', 'dropout_5/cond/dropout/mul', 'dropout_5/cond/Identity', 'dropout_5/cond/Identity/Switch', 'dropout_5/cond/Merge', 'output/kernel/Initializer/random_uniform/shape', 'output/kernel/Initializer/random_uniform/min', 'output/kernel/Initializer/random_uniform/max', 'output/kernel/Initializer/random_uniform/RandomUniform', 'output/kernel/Initializer/random_uniform/sub', 'output/kernel/Initializer/random_uniform/mul', 'output/kernel/Initializer/random_uniform', 'output/kernel', 'output/kernel/Assign', 'output/kernel/read', 'output/bias/Initializer/zeros/shape_as_tensor', 'output/bias/Initializer/zeros/Const', 'output/bias/Initializer/zeros', 'output/bias', 'output/bias/Assign', 'output/bias/read', 'output/MatMul', 'output/BiasAdd', 'Softmax', 'save/Const', 'save/SaveV2/tensor_names', 'save/SaveV2/shape_and_slices', 'save/SaveV2', 'save/control_dependency', 'save/RestoreV2/tensor_names', 'save/RestoreV2/shape_and_slices', 'save/RestoreV2', 'save/Assign', 'save/Assign_1', 'save/Assign_2', 'save/Assign_3', 'save/Assign_4', 'save/Assign_5', 'save/Assign_6', 'save/Assign_7', 'save/Assign_8', 'save/Assign_9', 'save/Assign_10', 'save/Assign_11', 'save/restore_all', 'init']
    freeze_graph(args.model_dir, args.output_node_names)