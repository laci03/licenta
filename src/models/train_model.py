import tensorflow as tf
import retrieveData as rd
import details as ds
import math
import numpy as np
import sys
from datetime import datetime
import neural_network_model as nnm


def conv2d(input, filters, ksize, name, strides=1):
    x = tf.layers.conv2d(input, filters=filters, kernel_size=ksize,
                         strides=strides, padding='SAME',
                         activation=tf.nn.relu, name=name)
    return x


def max_pool_2x2(input, ksize, strides, padding, name):
    x = tf.nn.max_pool(input,
                       ksize=ksize,
                       strides=strides,
                       padding=padding,
                       name=name)
    return x


def createDataSet():
    datasetPath = ds.training_path_street()
    (picture_files, classes) = rd.retrieveLabeledDataStreets(datasetPath)
    (path, data, label) = zip(*picture_files)

    validationDatasetPath = ds.validation_path_street()
    (validation_picture_files, _) = rd.retrieveLabeledDataStreets(validationDatasetPath)
    (validation_path, validation_data, validation_label) = zip(*validation_picture_files)

    data = [x for x in data]
    classes = [x for x in classes]
    path = [x for x in path]

    file = open(ds.street_classes_path(), 'w')

    for c in classes[:-1]:
        file.write(c + '\n')
    file.write(classes[-1])
    file.close()
    lookup = dict(zip(classes, range(len(classes))))
    print(lookup)
    label = [lookup[x] for x in label]

    print('total numbers of images: ', len(data))
    test_percentage = len(data) * 0.10
    data_for_test = math.trunc(len(data) / test_percentage)

    training_images = [x for x, y in zip(data, range(
        len(data))) if y % data_for_test != 0]
    training_labels = [x for x, y in zip(label, range(
        len(label))) if y % data_for_test != 0]
    training_paths = [x for x, y in zip(path, range(
        len(path))) if y % data_for_test != 0]

    print('training images: ', len(training_images))
    print('training labels: ', len(training_labels))
    print('training paths:', len(training_paths))

    testing_images = [x for x, y in zip(data, range(
        len(data))) if y % data_for_test == 0]
    testing_labels = [x for x, y in zip(label, range(
        len(label))) if y % data_for_test == 0]
    testing_paths = [x for x, y in zip(path, range(
        len(path))) if y % data_for_test == 0]

    print('testing images:', len(testing_images))
    print('testing labels:', len(testing_labels))
    print('testing paths:', len(testing_paths))

    validation_images = [x for x in validation_data]
    validation_paths = [x for x in validation_path]
    validation_labels = [lookup[x] for x in validation_label]

    print('validation images:', len(validation_images))
    print('validation labels:', len(validation_labels))
    print('validation paths:', len(validation_paths))

    batch_size = 16
    n_outputs = len(classes)

    tf.reset_default_graph()
    training_dataset = tf.data.Dataset.from_tensor_slices(
        (training_paths, training_images, training_labels))
    training_dataset = training_dataset.shuffle(len(training_images))
    training_dataset = training_dataset.apply(tf.contrib.data.map_and_batch(
        map_func=rd.decodeAndResize, batch_size=tf.placeholder_with_default(
            tf.constant(batch_size, dtype=tf.int64), ()),
        num_parallel_batches=1))
    training_dataset = training_dataset.prefetch(batch_size)

    testing_dataset = tf.data.Dataset.from_tensor_slices(
        (testing_paths, testing_images, testing_labels))
    testing_dataset = testing_dataset.apply(tf.contrib.data.map_and_batch(
        map_func=rd.decodeAndResize, batch_size=tf.placeholder_with_default(
            tf.constant(batch_size, dtype=tf.int64), ()),
        num_parallel_batches=1))
    testing_dataset = testing_dataset.prefetch(batch_size)

    validation_dataset = tf.data.Dataset.from_tensor_slices(
        (validation_paths, validation_images, validation_labels))
    validation_dataset = validation_dataset.apply(tf.contrib.data.map_and_batch(
        map_func=rd.decodeAndResize, batch_size=tf.placeholder_with_default(
            tf.constant(batch_size, dtype=tf.int64), ()),
        num_parallel_batches=1))
    validation_dataset = validation_dataset.prefetch(batch_size)

    return training_dataset, testing_dataset, validation_dataset, n_outputs


def train_CNN(scope, hm_epochs=3, saveEachEpoch=False, restore=False):
    training_dataset, testing_dataset, validation_dataset, n_outputs = createDataSet()

    iterator = tf.data.Iterator.from_structure(training_dataset.output_types,
                                               training_dataset.output_shapes)

    training_init_op = iterator.make_initializer(training_dataset, name='training_init')
    testing_init_op = iterator.make_initializer(testing_dataset, name='testing_init')
    validation_init_op = iterator.make_initializer(validation_dataset, name='validation_init')

    next_x, next_y = iterator.get_next()

    with tf.device('/gpu:0'):
        with tf.variable_scope(scope):
            dropout_rate_fc = 0.3
            training = tf.placeholder_with_default(False, shape=(), name='training')
            conv1 = conv2d(next_x, 128, 7, 'conv1')
            pool1 = max_pool_2x2(conv1, [1, 2, 2, 1], [1, 2, 2, 1], 'VALID', 'pool1')

            conv2_a1 = conv2d(pool1, 128, 1, 'conv2_inception_a1')
            conv2_a = conv2d(conv2_a1, 128, 3, 'conv2_inception_a')
            conv2_b1 = conv2d(pool1, 128, 1, 'conv2_inception_b1')
            conv2_b = conv2d(conv2_b1, 128, 7, 'conv2_inception_b')
            conv2_c1 = conv2d(pool1, 128, 1, 'conv2_inception_c1')
            conv2 = tf.concat([conv2_a, conv2_b, conv2_c1], 3)

            pool2 = max_pool_2x2(conv2, [1, 2, 2, 1], [1, 2, 2, 1], 'VALID', 'pool2')

            conv3 = conv2d(pool2, 256, 3, 'conv3', 2)
            pool3 = max_pool_2x2(conv3, [1, 2, 2, 1], [1, 2, 2, 1], 'VALID', 'pool3')

            conv4_a1 = conv2d(pool3, 256, 1, 'conv4_inception_a1')
            conv4_a = conv2d(conv4_a1, 256, 3, 'conv4_inception_a')
            conv4_b1 = conv2d(pool3, 256, 1, 'conv4_inception_b1')
            conv4_b = conv2d(conv4_b1, 256, 7, 'conv4_inception_b')
            conv4_c1 = conv2d(pool3, 256, 1, 'conv4_inception_c1')
            conv4 = tf.concat([conv4_a, conv4_b, conv4_c1], 3)

            pool4 = max_pool_2x2(conv4, [1, 2, 2, 1], [1, 2, 2, 1], 'VALID', 'pool4')
            conv5 = conv2d(pool4, 512, 5, 'conv5', 1)
            pool5 = max_pool_2x2(conv5, [1, 2, 2, 1], [1, 2, 2, 1], 'VALID', 'pool5')

            _, shape1, shape2, shape3 = pool5.shape
            pool5_flat = tf.reshape(pool5, shape=[-1, shape1 * shape2 * shape3])

            print('conv1.shape:', conv1.shape)
            print('pool1.shape:', pool1.shape)
            print('conv2.shape:', conv2.shape)
            print('pool2.shape', pool2.shape)
            # print('conv3.shape:', conv3.shape)
            print('conv4.shape', conv4.shape)
            print('pool3.shape', pool3.shape)
            print('conv5.shape', conv5.shape)
            print('pool4.shape', pool4.shape)

            fullyconn1 = tf.layers.dense(
                pool5_flat, 128, activation=tf.nn.relu, name='fc1')
            fullyconn2 = tf.layers.dense(
                fullyconn1, 256, activation=tf.nn.relu, name='fc2')
            fullyconn3 = tf.layers.dense(fullyconn2, 512, activation=tf.nn.relu, name='fc3')
            fc3_drop = tf.layers.dropout(fullyconn3, dropout_rate_fc, training=training)
            fullyconn4 = tf.layers.dense(fc3_drop, 128, activation=tf.nn.relu, name='fc4')

            logits = tf.layers.dense(fullyconn4, n_outputs, name='output')

    with tf.device('/gpu:0'):
        with tf.name_scope('cross_entropy'):
            xentropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
                logits=logits, labels=next_y)

        with tf.name_scope('loss_optimizer'):
            loss = tf.reduce_mean(xentropy)
            optimizer = tf.train.AdamOptimizer(learning_rate=0.0001)
            training_op = optimizer.minimize(loss)

        with tf.name_scope('accuracy'):
            correct = tf.nn.in_top_k(logits, next_y, 1)
            accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))
        tf.summary.scalar('training_accuracy', accuracy)

    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True
    saver = tf.train.Saver()

    with tf.Session(config=config) as sess:
        logPath = './tb_logs/'
        saved_modelsPath = ds.street_model_path()
        tbWriter = tf.summary.FileWriter(logPath, sess.graph)

        sess.run(tf.global_variables_initializer())

        if (restore):
            print('Model restored.')
            saver.restore(sess, saved_modelsPath + './last_street.ckpt')

        print('Training has started')
        for epoch in range(hm_epochs):
            epoch_start = datetime.now()
            sess.run(training_init_op)
            while True:
                try:
                    sess.run(training_op, feed_dict={training: True})
                except tf.errors.OutOfRangeError:
                    break
            if (saveEachEpoch):
                saver.save(sess, saved_modelsPath + 'epoch{0}.ckpt'.format(epoch))
                print('saved epoch', epoch)

            sess.run(testing_init_op)
            acc_test = 0
            counter = 0
            while True:
                try:
                    acc_test += accuracy.eval()
                    counter += 1
                except tf.errors.OutOfRangeError:
                    acc_test = acc_test / counter
                    break
            sess.run(validation_init_op)
            acc_validation = 0
            counter = 0
            while True:
                try:
                    acc_validation += sess.run(accuracy)
                    counter += 1
                except tf.errors.OutOfRangeError:
                    acc_validation = acc_validation / counter
                    print(
                        'Epoch {0}: Testing Accuracy: {1}, Validation accuracy {2}, Time taken {3}, Finished at {4}'.format(
                            epoch, acc_test, acc_validation, datetime.now() - epoch_start, datetime.now()))
                    break
            if ds.best_accuracy_street() < acc_validation:
                print('saved epoch as the best: {0} increase'.format(str(acc_validation - ds.best_accuracy_street())))
                file = open(ds.best_street_model_path() + 'best.txt', 'w')
                file.write(str(acc_validation))
                file.close()
                saver.save(sess, ds.best_street_saved_model())

        saver.save(sess, saved_modelsPath + './last_street.ckpt')


if __name__ == '__main__':
    try:
        start_time = datetime.now()
        restore = False
        saveEachEpoch = False
        hm_epochs = 1
        scope = ''
        if len(sys.argv) >= 2:
            scope = sys.argv[1]
        if len(sys.argv) >= 3:
            saveEachEpoch = sys.argv[2]
        if len(sys.argv) >= 4:
            restore = sys.argv[3]
        if len(sys.argv) >= 5:
            hm_epochs = int(sys.argv[4])

        train_CNN(scope, hm_epochs, saveEachEpoch, restore)
        print('Time taken:', datetime.now() - start_time)
    except Exception as e:
        print(
            'Sistemul s-a oprit deoarece parametrii introdusi sunt gresiti! Pentru mai multe detalii in legatura cu utilizarea scripturilor se poate consulta documentatia.')
