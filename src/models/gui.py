import sys

from PyQt5.QtWidgets import QProgressBar, QLabel, QHBoxLayout, QLineEdit, QPushButton, QWidget, QVBoxLayout, \
    QApplication, QMainWindow, QAction, qApp, QMessageBox, QInputDialog, QFileDialog, QComboBox
from PyQt5.QtGui import QFont
from PyQt5.QtCore import QThread, pyqtSignal
import details as ds
import cv2
import tensorflow as tf
import neural_network_model as nnm
import os
import retrieveData as rd
from threading import Thread

from CreateVideoThread import CreateVideoThread
from LabelFolderThread import LabelFolderThread
from PredictThread import PredictThread


class MenuDemo(QMainWindow):
    def __init__(self):
        super().__init__()
        self.__initWindow()
        self.__initNetwork()

    def initNetwork(self):
        tf.reset_default_graph()
        height, width, channels = ds.get_size()

        self.g1 = tf.Graph()
        with self.g1.as_default() as g:
            with g.name_scope('street'):
                self.X_street = tf.placeholder(tf.float32, shape=[
                    1, height, width, channels], name='X')
                self.street_outputs = ds.street_nr_of_outputs()
                self.street_model = ds.best_street_saved_model()
                self.street_classes = ds.street_classes()
                logits_streets = nnm.CNN_model(self.X_street, self.street_outputs, 'street')
                self.prediction_streets = tf.nn.softmax(logits_streets)
                self.saver_street = tf.train.Saver()
                self.street_init = tf.global_variables_initializer()

        tf.reset_default_graph()
        self.g2 = tf.Graph()
        with self.g2.as_default() as g:
            with g.name_scope('segment'):
                self.X_segment = tf.placeholder(tf.float32, shape=[
                    1, height, width, channels], name='X')
                self.segment_outputs = ds.segment_nr_of_outputs()
                self.segment_model = ds.best_segment_saved_model()
                self.segment_classes = ds.segment_classes()
                logits_segments = nnm.CNN_model(self.X_segment, self.segment_outputs, 'segment')
                self.prediction_segments = tf.nn.softmax(logits_segments)
                self.saver_segment = tf.train.Saver()
                self.segment_init = tf.global_variables_initializer()

    def initPredict(self):
        self.inferenceTitle = QLabel('Image localization')
        self.inferenceTitle.setFont(QFont('Times', weight=QFont.Bold))

        h_inferenceTitle = QHBoxLayout()
        h_inferenceTitle.addStretch()
        h_inferenceTitle.addWidget(self.inferenceTitle)
        h_inferenceTitle.addStretch()

        self.predictionPath = QLineEdit()
        self.clearPredict = QPushButton('Clear')
        self.predict = QPushButton('Predict')
        self.browsePredict = QPushButton('Browse')

        self.inferenceMode = QLabel('Inference mode:')
        self.inferenceModelPredict = QComboBox()
        self.inferenceModelPredict.addItems(['Street', 'Segment', 'Both'])

        self.v_box = QVBoxLayout()
        self.v_box.addLayout(h_inferenceTitle)
        self.v_box.addWidget(self.predictionPath)
        h_boxInference = QHBoxLayout()
        h_boxInference.addWidget(self.inferenceMode)
        h_boxInference.addWidget(self.inferenceModelPredict)
        h_boxInference.addWidget(self.predict)
        h_boxInference.addWidget(self.browsePredict)
        h_boxInference.addWidget(self.clearPredict)
        self.v_box.addLayout(h_boxInference)

    def initCreateVideo(self):
        self.createTitle = QLabel('Create Video')
        self.createTitle.setFont(QFont('Times', weight=QFont.Bold))

        h_createTitle = QHBoxLayout()
        h_createTitle.addStretch()
        h_createTitle.addWidget(self.createTitle)
        h_createTitle.addStretch()
        self.v_box.addLayout(h_createTitle)

        self.createvideo_path = QLineEdit()
        self.clearCreateVideo = QPushButton('Clear')
        self.createVideo = QPushButton('Create Video')
        self.browseCreateVideo = QPushButton('Browse')
        self.progressBarVideo = QProgressBar()

        self.inferenceModelVideo = QComboBox()
        self.inferenceModelVideo.addItems(['Street', 'Segment', 'Both'])
        self.inferenceMode1 = QLabel('Inference mode:')
        self.v_box.addWidget(self.progressBarVideo)
        self.v_box.addWidget(self.createvideo_path)
        h_box_create_video = QHBoxLayout()
        h_box_create_video.addWidget(self.inferenceMode1)
        h_box_create_video.addWidget(self.inferenceModelVideo)
        h_box_create_video.addWidget(self.createVideo)
        h_box_create_video.addWidget(self.browseCreateVideo)
        h_box_create_video.addWidget(self.clearCreateVideo)
        self.v_box.addLayout(h_box_create_video)

        self.labelingPath = QLineEdit()
        self.clearLabelFolder = QPushButton('Clear')
        self.label = QPushButton('Label Folder')
        self.browseLabelFolder = QPushButton('Browse')
        self.progressBarLabeling = QProgressBar()
        self.v_box.addWidget(self.createvideo_path)

    def initFolderLabeling(self):
        self.labelTitle = QLabel('Label all images')
        self.labelTitle.setFont(QFont('Times', weight=QFont.Bold))

        h_labelTitle = QHBoxLayout()
        h_labelTitle.addStretch()
        h_labelTitle.addWidget(self.labelTitle)
        h_labelTitle.addStretch()
        self.v_box.addLayout(h_labelTitle)
        self.v_box.addWidget(self.progressBarLabeling)
        self.v_box.addWidget(self.labelingPath)
        h_box_label = QHBoxLayout()
        h_box_label.addWidget(self.label)
        h_box_label.addWidget(self.browseLabelFolder)
        h_box_label.addWidget(self.clearLabelFolder)
        self.v_box.addLayout(h_box_label)

        self.wid = QWidget(self)
        self.setCentralWidget(self.wid)
        self.wid.setLayout(self.v_box)

        self.clearPredict.clicked.connect(self.clear_prediction_path)
        self.clearCreateVideo.clicked.connect(self.clear_create_video_path)
        self.clearLabelFolder.clicked.connect(self.clear_label_folder_path)
        self.predict.clicked.connect(self.inference)
        self.createVideo.clicked.connect(self.create_video)
        self.label.clicked.connect(self.label_images)
        self.browsePredict.clicked.connect(self.select_file)
        self.browseCreateVideo.clicked.connect(self.select_folder_cv)
        self.browseLabelFolder.clicked.connect(self.select_folder_label)

    def initWindow(self):
        self.initPredict()
        self.initCreateVideo()
        self.initFolderLabeling()

        self.setWindowTitle('Find my location')
        self.show()

    __initWindow = initWindow
    __initNetwork = initNetwork

    def clear_prediction_path(self):
        self.predictionPath.clear()

    def clear_create_video_path(self):
        self.createvideo_path.clear()

    def clear_label_folder_path(self):
        self.labelingPath.clear()

    def inference(self):
        mode = self.inferenceModelPredict.currentIndex()
        path = self.predictionPath.text()

        self.predictThread = PredictThread(path, mode)
        self.predictThread.start()

    def create_video(self):
        path = self.createvideo_path.text()
        mode = self.inferenceModelVideo.currentIndex()
        self.myThread = CreateVideoThread(path, mode)
        self.myThread.sig_done.connect(self.done_video)
        self.myThread.sig_percent.connect(self.set_progress_bar_video)
        self.myThread.start()

    def set_progress_bar_video(self, percent):
        self.progressBarVideo.setValue(percent)

    def done_video(self):
        QMessageBox.question(self, 'Done', 'You can find the video at d:/video.mp4', QMessageBox.Ok, QMessageBox.Ok)
        self.progressBarVideo.setValue(0)

    def set_progress_bar_label(self, percent):
        self.progressBarLabeling.setValue(percent)

    def done_label(self, path):
        QMessageBox.question(self, 'Done', 'You can find the labeled images at ' + path, QMessageBox.Ok, QMessageBox.Ok)
        self.progressBarLabeling.setValue(0)

    def label_images(self):
        path = self.labelingPath.text()

        self.labelThread = LabelFolderThread(path)
        self.labelThread.sig_done_label.connect(self.done_label)
        self.labelThread.sig_percent_label.connect(self.set_progress_bar_label)
        self.labelThread.start()

    def select_file(self):
        fileName, _ = QFileDialog.getOpenFileName(self, 'Select files', 'd:/', 'jpg(*.jpg)')
        self.predictionPath.setText(fileName)

    def select_folder_cv(self):
        folder = str(QFileDialog.getExistingDirectory(self, 'Select Directory', 'd:/'))
        if folder != '' and folder[-1] != '/':
            folder = folder + '/'
        self.createvideo_path.setText(folder)

    def select_folder_label(self):
        folder = str(QFileDialog.getExistingDirectory(self, 'Select Directory', 'd:/'))
        if folder != '' and folder[-1] != '/':
            folder = folder + '/'
        self.labelingPath.setText(folder)


app = QApplication(sys.argv)
menus = MenuDemo()
sys.exit(app.exec_())
