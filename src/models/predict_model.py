import tensorflow as tf
import numpy as np
import sys
import retrieveData as rd
import cv2
import os
import details as ds
import neural_network_model as nnm
from datetime import datetime


def predict(path, mode=0):
    if mode == 0:
        n_outputs = ds.street_nr_of_outputs()
        classes = ds.street_classes()
        scope = 'street'
        saved_model = ds.best_street_saved_model()
    elif mode == 1:
        n_outputs = ds.segment_nr_of_outputs()
        classes = ds.segment_classes()
        scope = 'segment'
        saved_model = ds.best_segment_saved_model()

    tf.reset_default_graph()

    height, width, channels = ds.get_size()

    X = tf.placeholder(tf.float32, shape=[
        1, height, width, channels], name='X')
    logits = nnm.CNN_model(X, n_outputs, scope)
    prediction = tf.nn.softmax(logits)

    config = tf.ConfigProto(allow_soft_placement=True)
    saver = tf.train.Saver()
    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        saver.restore(sess, saved_model)
        print('Model restored.')
        probabilities = sess.run(prediction, feed_dict={X: nnm.getImage(path).eval()})[0]
        probabilities = dict(zip(classes, probabilities))
        arrayProbabilities = sorted(probabilities.items(), key=lambda x: x[1], reverse=True)
        probabilities = dict(arrayProbabilities)
        print(probabilities)
    return arrayProbabilities[0]


def labelPhotos(path, output_path):
    streets = ds.street_classes()
    segments = ds.segment_classes()
    if os.path.isdir(output_path) == False:
        os.mkdir(output_path)
    for street in streets:
        outputstreet_path = output_path + street + '/'
        if os.path.isdir(outputstreet_path) == False:
            os.mkdir(output_path + street)
        for segment in segments:
            outputSegmenthPath = outputstreet_path + segment
            if os.path.isdir(outputSegmenthPath) == False:
                os.mkdir(outputSegmenthPath)

    tf.reset_default_graph()
    height, width, channels = ds.get_size()

    images = rd.retrieve_data(path)
    print(len(images))
    print(path)
    g1 = tf.Graph()

    with g1.as_default() as g:
        with g.name_scope('street'):
            X_street = tf.placeholder(tf.float32, shape=[
                1, height, width, channels], name='X')
            street_outputs = ds.street_nr_of_outputs()
            street_model = ds.best_street_saved_model()
            street_classes = ds.street_classes()
            logits_streets = nnm.CNN_model(X_street, street_outputs, 'street')
            prediction_streets = tf.nn.softmax(logits_streets)
            saver_street = tf.train.Saver()
            street_init = tf.global_variables_initializer()

    tf.reset_default_graph()
    g2 = tf.Graph()
    with g2.as_default() as g:
        with g.name_scope('segment'):
            X_segment = tf.placeholder(tf.float32, shape=[
                1, height, width, channels], name='X')
            segment_outputs = ds.segment_nr_of_outputs()
            segment_model = ds.best_segment_saved_model()
            segment_classes = ds.segment_classes()
            logits_segments = nnm.CNN_model(X_segment, segment_outputs, 'segment')
            prediction_segments = tf.nn.softmax(logits_segments)
            saver_segment = tf.train.Saver()
            segment_init = tf.global_variables_initializer()

    config = tf.ConfigProto(allow_soft_placement=True)
    with tf.Session(config=config, graph=g1) as sess_street:
        with tf.Session(config=config, graph=g2) as sess_segment:
            sess_street.run(street_init)
            sess_segment.run(segment_init)

            saver_segment.restore(sess_segment, segment_model)
            saver_street.restore(sess_street, street_model)
            print('Model restored.')

            counter = 0
            last_percent = 0
            current_percent = 0
            length = len(images)

            for image in images:
                current_percent = counter / length
                if last_percent * 100 + 1 < current_percent * 100:
                    print('{0}%'.format(int(current_percent * 100)), end='\r')
                    last_percent = current_percent

                img = cv2.imread(path + image)
                probabilities_street = sess_street.run(prediction_streets, feed_dict={
                    X_street: nnm.getImage(path + image).eval()})[0]
                _, max_idx_street = max([(v, i) for i, v in enumerate(probabilities_street)])

                probabilities_segment = sess_segment.run(prediction_segments, feed_dict={
                    X_segment: nnm.getImage(path + image).eval()})[0]
                _, max_idx_segments = max([(v, i) for i, v in enumerate(probabilities_segment)])

                street_name = street_classes[max_idx_street]
                segment_nr = segment_classes[max_idx_segments]

                cv2.imwrite(output_path + street_name + '/' + segment_nr + '/' + image, img)
                counter = counter + 1

    print('Done')


if __name__ == '__main__':
    try:
        mode = 0
        if len(sys.argv) >= 3:
            mode = int(sys.argv[2])
        predict(sys.argv[1], mode)
    except Exception as e:
        print(
            'Sistemul s-a oprit deoarece parametrii introdusi sunt gresiti! Pentru mai multe detalii in legatura cu utilizarea scripturilor se poate consulta documentatia.')
