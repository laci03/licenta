import sys

from PyQt5.QtCore import QThread, pyqtSignal
import details as ds
import cv2
import tensorflow as tf
import neural_network_model as nnm
import os
import retrieveData as rd
from threading import Thread


class CreateVideoThread(QThread):
    sig_done = pyqtSignal(str)
    sig_percent = pyqtSignal(int)

    def __init__(self, path, mode):
        QThread.__init__(self)
        self.path = path
        self.mode = mode
        self.__initNetwork()

    def __del__(self):
        self.wait()

    def run(self):
        name = 'video.mp4'
        fps = 10
        step = 4
        output_path = 'd:/'
        images = rd.retrieve_data(self.path)
        img1 = cv2.imread(self.path + images[0])
        videoHeight, videoWidth, _ = img1.shape
        fourcc = cv2.VideoWriter_fourcc(*'DIVX')
        video = cv2.VideoWriter(output_path + name, fourcc, fps, (videoWidth, videoHeight))

        counter = 0
        last_percent = 0
        current_percent = 0
        length = len(images)

        font = cv2.FONT_HERSHEY_SIMPLEX
        bottom_left_corner_of_text = (400, 100)
        font_scale = 3
        fontColor = (0, 0, 255)
        lineType = 4
        if self.mode == 0:
            config = tf.ConfigProto(allow_soft_placement=True)

            with tf.Session(config=config, graph=self.g1) as sess_street:
                sess_street.run(self.street_init)
                self.saver_street.restore(sess_street, self.street_model)
                print('Model restored.')
                for image in images:
                    if counter % step == 0:
                        current_percent = counter / length
                        if last_percent * 100 + 1 < current_percent * 100:
                            print('{0}%'.format(int(current_percent * 100)), end='\r')
                            self.sig_percent.emit(int(current_percent * 100))
                            last_percent = current_percent

                        img = cv2.imread(self.path + image)

                        probabilities_street = sess_street.run(self.prediction_streets, feed_dict={
                            self.X_street: nnm.getImage(self.path + image).eval()})[0]
                        _, max_idx = max([(v, i) for i, v in enumerate(probabilities_street)])

                        cv2.putText(img, self.street_classes[max_idx],
                                    bottom_left_corner_of_text,
                                    font,
                                    font_scale,
                                    fontColor,
                                    lineType)
                        video.write(img)

                        cv2.destroyAllWindows()
                    counter = counter + 1
                video.release()
        elif self.mode == 1:
            config = tf.ConfigProto(allow_soft_placement=True)

            with tf.Session(config=config, graph=self.g2) as sess_segment:
                sess_segment.run(self.segment_init)
                self.saver_segment.restore(sess_segment, self.segment_model)
                print('Model restored.')
                for image in images:
                    if counter % step == 0:
                        current_percent = counter / length
                        if last_percent * 100 + 1 < current_percent * 100:
                            print('{0}%'.format(int(current_percent * 100)), end='\r')
                            self.sig_percent.emit(int(current_percent * 100))
                            last_percent = current_percent

                        img = cv2.imread(self.path + image)

                        probabilities_segment = sess_segment.run(self.prediction_segments, feed_dict={
                            self.X_segment: nnm.getImage(self.path + image).eval()})[0]
                        _, max_idx = max([(v, i) for i, v in enumerate(probabilities_segment)])

                        cv2.putText(img, self.segment_classes[max_idx],
                                    bottom_left_corner_of_text,
                                    font,
                                    font_scale,
                                    fontColor,
                                    lineType)
                        video.write(img)

                        cv2.destroyAllWindows()
                    counter = counter + 1
                video.release()
        elif self.mode == 2:

            everything = ds.coordinates()
            config = tf.ConfigProto(allow_soft_placement=True)
            with tf.Session(config=config, graph=self.g1) as sess_street:
                with tf.Session(config=config, graph=self.g2) as sess_segment:
                    sess_street.run(self.street_init)
                    sess_segment.run(self.segment_init)

                    self.saver_segment.restore(sess_segment, self.segment_model)
                    self.saver_street.restore(sess_street, self.street_model)
                    print('Model restored.')

                    for image in images:
                        if counter % step == 0:
                            current_percent = counter / length
                            if last_percent * 100 + 1 < current_percent * 100:
                                print('{0}%'.format(int(current_percent * 100)), end='\r')
                                self.sig_percent.emit(int(current_percent * 100))
                                last_percent = current_percent

                            img = cv2.imread(self.path + image)
                            probabilities_street = sess_street.run(self.prediction_streets, feed_dict={
                                self.X_street: nnm.getImage(self.path + image).eval()})[0]
                            _, max_idx_street = max([(v, i) for i, v in enumerate(probabilities_street)])

                            probabilities_segment = sess_segment.run(self.prediction_segments, feed_dict={
                                self.X_segment: nnm.getImage(self.path + image).eval()})[0]
                            _, max_idx_segments = max([(v, i) for i, v in enumerate(probabilities_segment)])

                            street_name = self.street_classes[max_idx_street]
                            segment_nr = self.segment_classes[max_idx_segments]
                            cv2.putText(img, '{0} {1}'.format(street_name, segment_nr),
                                        bottom_left_corner_of_text,
                                        font,
                                        font_scale,
                                        fontColor,
                                        lineType)

                            cv2.putText(img, '{0}'.format(everything[street_name][int(segment_nr)]),
                                        (400, 200),
                                        font,
                                        font_scale,
                                        fontColor,
                                        lineType)
                            video.write(img)

                            cv2.destroyAllWindows()

                        counter = counter + 1
                    video.release()
        self.sig_done.emit('Done!')

    def initNetwork(self):
        tf.reset_default_graph()
        height, width, channels = ds.get_size()

        self.g1 = tf.Graph()
        with self.g1.as_default() as g:
            with g.name_scope('street'):
                self.X_street = tf.placeholder(tf.float32, shape=[
                    1, height, width, channels], name='X')
                self.street_outputs = ds.street_nr_of_outputs()
                self.street_model = ds.best_street_saved_model()
                self.street_classes = ds.street_classes()
                logits_streets = nnm.CNN_model(self.X_street, self.street_outputs, 'street')
                self.prediction_streets = tf.nn.softmax(logits_streets)
                self.saver_street = tf.train.Saver()
                self.street_init = tf.global_variables_initializer()

        tf.reset_default_graph()
        self.g2 = tf.Graph()
        with self.g2.as_default() as g:
            with g.name_scope('segment'):
                self.X_segment = tf.placeholder(tf.float32, shape=[
                    1, height, width, channels], name='X')
                self.segment_outputs = ds.segment_nr_of_outputs()
                self.segment_model = ds.best_segment_saved_model()
                self.segment_classes = ds.segment_classes()
                logits_segments = nnm.CNN_model(self.X_segment, self.segment_outputs, 'segment')
                self.prediction_segments = tf.nn.softmax(logits_segments)
                self.saver_segment = tf.train.Saver()
                self.segment_init = tf.global_variables_initializer()

    __initNetwork = initNetwork
