import tensorflow as tf
import cv2
from datetime import datetime
import sys
import neural_network_model as nnm
import details as ds
import os
import numpy as np
import retrieveData as rd
import matplotlib.pyplot as plt
import seaborn as sns
import argparse

segments_histograms = './segments_histograms'
streets_histograms = './streets_histograms'


def get_distribution(path, label, mode):
    distribution = []
    photos = rd.retrieve_data(path)
    photos = [path + x for x in photos]

    if mode == 0:
        classes = ds.street_classes()
        saved_model = ds.best_street_saved_model()
        scope = 'street'
    else:
        classes = ds.segment_classes()
        saved_model = ds.best_segment_saved_model()
        scope = 'segment'

    batch_size = 16

    n_outputs = len(classes)
    tf.reset_default_graph()
    testing_dataset = tf.data.Dataset.from_tensor_slices(photos)
    testing_dataset = testing_dataset.apply(tf.contrib.data.map_and_batch(
        map_func=nnm.getImageBatch, batch_size=tf.placeholder_with_default(
            tf.constant(batch_size, dtype=tf.int64), ()),
        num_parallel_batches=1))
    testing_dataset = testing_dataset.prefetch(batch_size)

    iterator = tf.data.Iterator.from_structure(testing_dataset.output_types, testing_dataset.output_shapes)

    testing_init_op = iterator.make_initializer(testing_dataset, name='testing_init')
    next_x = iterator.get_next()

    logits = nnm.CNN_model(next_x, n_outputs, scope)

    with tf.device('/gpu:0'):
        prediction = tf.nn.softmax(logits)

    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True
    saver = tf.train.Saver()

    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        saver.restore(sess, saved_model)
        print('Model restored.')
        print('Creating histogram, started!')

        sess.run(testing_init_op)

        last_percent = 0
        current_percent = 0
        counter = 0

        length = len(photos) / batch_size
        while True:
            try:
                current_percent = counter / length
                if last_percent * 100 + 1 < current_percent * 100:
                    print('{0}%'.format(int(current_percent * 100)), end='\r')
                    last_percent = current_percent

                pred = prediction.eval()
                for i in range(len(pred)):
                    segment_probability = pred[i][label]
                    distribution.append(int(segment_probability * 100))
                counter += 1
            except tf.errors.OutOfRangeError:
                print('Done for', path)
                break
    return distribution


def create_histogram_segment(input_path, output_path):
    streets = os.listdir(input_path)
    all_distribution = []
    file = open(output_path + 'stats.txt', 'w')

    for street in streets:
        street_path = input_path + street
        segments = os.listdir(street_path)
        street_histogram = []

        for segment in segments:
            segment_path = street_path + '/' + segment + '/'
            distribution = get_distribution(segment_path, int(segment), 1)
            street_histogram.extend(distribution)
            all_distribution.extend(distribution)
            sns.set()

            bins = range(0, 101)
            plt.hist(distribution, bins, rwidth=0.8)
            plt.title('Histogram for street:{0} segment {1}'.format(street, segment))
            plt.xlabel('Probability of the correct label')
            plt.ylabel('Frequency')
            plt.savefig(output_path + '{0}_{1}.png'.format(street, segment))
            plt.close()

        segment_groups, segment_groups_percentage = create_groups(street_histogram)
        file.write(street + ': ' + str(segment_groups) + '\n')
        file.write(street + ': ' + str(segment_groups_percentage) + '\n')

        plt.hist(street_histogram, bins, rwidth=0.8)
        plt.title('Histogram for street:{0}'.format(street))
        plt.xlabel('Probability of the correct label')
        plt.ylabel('Frequency')
        plt.savefig(output_path + 'segment_histogram_{0}.png'.format(street))
        plt.close()

    segment_groups, segment_groups_percentage = create_groups(all_distribution)
    file.write('total' + ': ' + str(segment_groups) + '\n')
    file.write('total' + ': ' + str(segment_groups_percentage) + '\n')

    plt.hist(segment_groups, range(0, 5), rwidth=1)

    plt.title('Histogram for all segments')
    plt.xlabel('Probability of the correct label')
    plt.ylabel('Frequency')
    plt.savefig(output_path + 'segmentAll5.png')
    plt.close()

    plt.hist(all_distribution, bins, rwidth=0.8)
    plt.title('Histogram for all segments')
    plt.xlabel('Probability of the correct label')
    plt.ylabel('Frequency')
    plt.savefig(output_path + 'segment_histogram_all.png')
    plt.close()

    file.close()


def create_groups(input_array, nr_of_groups=5):
    output = np.zeros(nr_of_groups, int)
    boundary = 100.0 / nr_of_groups
    total_elements = len(input_array)
    for i in range(0, total_elements):
        group_index = int(input_array[i] / boundary)
        if group_index == nr_of_groups:
            group_index -= 1
        output[group_index] += 1
    output_percentage = [x / float(total_elements) for x in output]
    return output, output_percentage


def create_histogram_street(input_path, output_path):
    streets = os.listdir(input_path)
    all_distribution = []
    file = open(output_path + 'stats.txt', 'w')

    for street in streets:
        classes = ds.street_classes()
        lookup = dict(zip(classes, range(len(classes))))
        street_path = input_path + street + '/'
        street_histogram = get_distribution(street_path, lookup[street], 0)
        all_distribution.extend(street_histogram)

        street_groups, street_groups_percentage = create_groups(street_histogram)
        file.write(street + ': ' + str(street_groups_percentage) + '\n')
        file.write(street + ': ' + str(street_groups) + '\n')

        sns.set()
        bins = range(0, 101)

        plt.hist(street_histogram, bins, rwidth=0.8)
        plt.title('Histogram for street:{0}'.format(street))
        plt.xlabel('Probability of the correct label')
        plt.ylabel('Frequency')
        plt.savefig(output_path + 'street_{0}.png'.format(street))
        plt.close()

    plt.hist(all_distribution, bins, rwidth=0.8)
    plt.title('Histogram for all streets')
    plt.xlabel('Probability of the correct label')
    plt.ylabel('Frequency')
    plt.savefig(output_path + 'street_histogram_all.png')
    plt.close()

    street_groups, street_groups_percentage = create_groups(all_distribution)
    file.write('total' + ': ' + str(street_groups_percentage) + '\n')
    file.write('total' + ': ' + str(street_groups) + '\n')

    file.close()


if __name__ == '__main__':
    try:
        start_time = datetime.now()

        parser = argparse.ArgumentParser()
        parser.add_argument('--input_path', default='G:/Laci/Licenta/Licenta_teste/original/test_bogdan_smailo/')
        parser.add_argument('--output_path', default='./')
        parser.add_argument('--mode', default='0')

        args = parser.parse_args()

        if int(args.mode) == 0:
            create_histogram_street(args.input_path, args.output_path)
        else:
            create_histogram_segment(args.input_path, args.output_path)
        print('Time taken:', datetime.now() - start_time)
    except Exception as e:
        print(e)
        print(
            'Sistemul s-a oprit deoarece parametrii introdusi sunt gresiti! Pentru mai multe detalii in legatura cu utilizarea scripturilor se poate consulta documentatia.')
