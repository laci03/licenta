import sys

from PyQt5.QtCore import QThread, pyqtSignal
import details as ds
import cv2
import tensorflow as tf
import neural_network_model as nnm
import os
import retrieveData as rd
from threading import Thread


class LabelFolderThread(QThread):
    sig_done_label = pyqtSignal(str)
    sig_percent_label = pyqtSignal(int)

    def __init__(self, path):
        QThread.__init__(self)
        self.path = path
        self.__initNetwork()

    def __del__(self):
        self.wait()

    def run(self):
        config = tf.ConfigProto(allow_soft_placement=True)
        output_path = self.path[:-1] + 'Labeled/'
        if os.path.isdir(output_path) == False:
            os.mkdir(output_path)
        streets = ds.street_classes()
        segments = ds.segment_classes()
        for street in streets:
            path_to_street = output_path + '{0}/'.format(street)
            if os.path.isdir(path_to_street) == False:
                os.mkdir(path_to_street)
            for segment in segments:
                path_to_segment = path_to_street + '{0}/'.format(segment)
                if os.path.isdir(path_to_segment) == False:
                    os.mkdir(path_to_segment)

        with tf.Session(config=config, graph=self.g1) as sess_street:
            with tf.Session(config=config, graph=self.g2) as sess_segment:
                sess_street.run(self.street_init)
                sess_segment.run(self.segment_init)

                self.saver_segment.restore(sess_segment, self.segment_model)
                self.saver_street.restore(sess_street, self.street_model)
                print('Model restored.')

                images = rd.retrieve_data(self.path)

                counter = 0
                last_percent = 0
                current_percent = 0
                length = len(images)

                for image in images:
                    current_percent = counter / length
                    if last_percent * 100 + 1 < current_percent * 100:
                        print('{0}%'.format(int(current_percent * 100)), end='\r')
                        self.sig_percent_label.emit(int(current_percent * 100))
                        last_percent = current_percent

                    img = cv2.imread(self.path + image)
                    probabilities_street = sess_street.run(self.prediction_streets, feed_dict={
                        self.X_street: nnm.getImage(self.path + image).eval()})[0]
                    _, max_idx_street = max([(v, i) for i, v in enumerate(probabilities_street)])

                    probabilities_segment = sess_segment.run(self.prediction_segments, feed_dict={
                        self.X_segment: nnm.getImage(self.path + image).eval()})[0]
                    _, max_idx_segments = max([(v, i) for i, v in enumerate(probabilities_segment)])

                    street_name = self.street_classes[max_idx_street]
                    segment_nr = self.segment_classes[max_idx_segments]

                    cv2.imwrite(output_path + street_name + '/' + segment_nr + '/' + image, img)
                    counter = counter + 1
        self.sig_done_label.emit(output_path)

    def initNetwork(self):
        tf.reset_default_graph()
        height, width, channels = ds.get_size()

        self.g1 = tf.Graph()
        with self.g1.as_default() as g:
            with g.name_scope('street'):
                self.X_street = tf.placeholder(tf.float32, shape=[
                    1, height, width, channels], name='X')
                self.street_outputs = ds.street_nr_of_outputs()
                self.street_model = ds.best_street_saved_model()
                self.street_classes = ds.street_classes()
                logits_streets = nnm.CNN_model(self.X_street, self.street_outputs, 'street')
                self.prediction_streets = tf.nn.softmax(logits_streets)
                self.saver_street = tf.train.Saver()
                self.street_init = tf.global_variables_initializer()

        tf.reset_default_graph()
        self.g2 = tf.Graph()
        with self.g2.as_default() as g:
            with g.name_scope('segment'):
                self.X_segment = tf.placeholder(tf.float32, shape=[
                    1, height, width, channels], name='X')
                self.segment_outputs = ds.segment_nr_of_outputs()
                self.segment_model = ds.best_segment_saved_model()
                self.segment_classes = ds.segment_classes()
                logits_segments = nnm.CNN_model(self.X_segment, self.segment_outputs, 'segment')
                self.prediction_segments = tf.nn.softmax(logits_segments)
                self.saver_segment = tf.train.Saver()
                self.segment_init = tf.global_variables_initializer()

    __initNetwork = initNetwork
