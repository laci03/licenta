import os
import sys
import tensorflow as tf
import details as ds
import math

from datetime import datetime

height, width, channels = ds.get_size()


def retrieveLabeledDataStreets(dir):
    start_time = datetime.now()
    streets = os.listdir(dir)
    print('Start retrieving labeled data!')

    picture_files = []
    for street in streets:
        images = [(dir + '{0}/'.format(street), name, street)
                  for name in os.listdir(dir + '{0}/'.format(street)) if name[-4:] == '.jpg']
        picture_files.extend(images)
    print('Done retrieving labeled data!')
    print('Time taken:', datetime.now() - start_time)
    return picture_files, streets


def retrieveLabeledDataSegments(dir):
    start_time = datetime.now()
    streets = os.listdir(dir)
    print('Start retrieving labeled data!')

    picture_files = []
    for street in streets:
        # if 'garii' not in street and 'stadionului' not in street and 'morii' not in street:
        #     continue

        street_path = dir + street + '/'
        segments = os.listdir(street_path)
        segments = sorted(segments, key=lambda a: int(a))
        for segment in segments:
            images = [(dir + '{0}/'.format(street), name, segment, street)
                      for name in os.listdir(street_path + '{0}/'.format(segment)) if name[-4:] == '.jpg']
            picture_files.extend(images)
    print('Done retrieving labeled data!')
    print('Time taken:', datetime.now() - start_time)
    return picture_files, segments


def retrieve_data(dir):
    images = [name for name in os.listdir(dir) if name[-4:] == '.jpg']
    print('Done retrieving data!')
    return images


def decodeAndResize(path, filename, label):
    image_string = tf.read_file(path + filename)
    image_decoded = tf.image.decode_jpeg(image_string, channels=channels, dct_method='INTEGER_ACCURATE')
    image_resized = tf.image.resize_images(image_decoded, [height, width])
    image_resized.set_shape([height, width, channels])
    image_rgb = image_resized[:, :, ::-1]

    return image_rgb, label


def decodeAndResizeSegment(path, filename, label, label_int):
    image_string = tf.read_file(path + label + '/' + filename)
    image_decoded = tf.image.decode_jpeg(image_string, channels=channels, dct_method='INTEGER_ACCURATE')
    image_resized = tf.image.resize_images(image_decoded, [height, width])
    image_resized.set_shape([height, width, channels])
    image_rgb = image_resized[:, :, ::-1]

    return image_rgb, label_int
