import tensorflow as tf
import retrieveData as rd
import details as ds
import math
import numpy as np
import sys
from datetime import datetime
import neural_network_model as nnm

saved_modelsPath = ds.street_model_path()


def test(path, model):
    (picture_files, _) = rd.retrieveLabeledDataStreets(path)
    (path, data, label) = zip(*picture_files)
    testing_images = [x for x in data]
    classes = ds.street_classes()

    testing_paths = [x for x in path]
    lookup = dict(zip(classes, range(len(classes))))
    print(lookup)
    testing_labels = [lookup[x] for x in label]

    print('testing images:', len(testing_images))

    batch_size = 16
    n_outputs = len(classes)

    tf.reset_default_graph()
    testing_dataset = tf.data.Dataset.from_tensor_slices(
        (testing_paths, testing_images, testing_labels))
    testing_dataset = testing_dataset.apply(tf.contrib.data.map_and_batch(
        map_func=rd.decodeAndResize, batch_size=tf.placeholder_with_default(
            tf.constant(batch_size, dtype=tf.int64), ()),
        num_parallel_batches=1))
    testing_dataset = testing_dataset.prefetch(batch_size)

    iterator = tf.data.Iterator.from_structure(testing_dataset.output_types,
                                               testing_dataset.output_shapes)

    testing_init_op = iterator.make_initializer(testing_dataset, name='testing_init')
    next_x, next_y = iterator.get_next()
    logits = nnm.CNN_model(next_x, n_outputs, 'street')

    with tf.device('/gpu:0'):
        correct = tf.nn.in_top_k(logits, next_y, 1)
        accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True
    saver = tf.train.Saver()

    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        saver.restore(sess, model)
        print('Model restored.')
        print('Testing has started')
        sess.run(testing_init_op)
        acc_test = 0
        counter = 0
        last_percent = 0
        current_percent = 0
        length = len(testing_images) / batch_size
        while True:
            try:
                current_percent = counter / length
                if last_percent * 100 + 1 < current_percent * 100:
                    print('{0}%'.format(int(current_percent * 100)), end='\r')
                    last_percent = current_percent

                acc_test += accuracy.eval()
                counter += 1
            except tf.errors.OutOfRangeError:
                print('Testing accuracy {0}'.format(acc_test / counter))
                break


if __name__ == '__main__':
    try:
        start_time = datetime.now()
        path = ds.validation_path_street()
        model = ds.best_street_saved_model()
        if len(sys.argv) >= 2:
            path = sys.argv[1]
        if len(sys.argv) >= 3:
            model = sys.argv[2]
        # print(model)
        test(path, model)
        print('Time taken:', datetime.now() - start_time)
    except Exception as e:
        print(
            'Sistemul s-a oprit deoarece parametrii introdusi sunt gresiti! Pentru mai multe detalii in legatura cu utilizarea scripturilor se poate consulta documentatia.')
