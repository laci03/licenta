import tensorflow as tf
import details as ds

height, width, channels = ds.get_size()


def conv2d(input, filters, ksize, name, strides=1):
    x = tf.layers.conv2d(input, filters=filters, kernel_size=ksize,
                         strides=strides, padding='SAME',
                         activation=tf.nn.relu, name=name)
    return x


def max_pool_2x2(input, ksize, strides, padding, name):
    x = tf.nn.max_pool(input,
                       ksize=ksize,
                       strides=strides,
                       padding=padding,
                       name=name)
    return x


def CNN_model(x, n_outputs, scope):
    with tf.device('/gpu:0'):
        with tf.variable_scope(scope):
            dropout_rate_fc = 0.3
            training = tf.placeholder_with_default(False, shape=(), name='training')
            conv1 = conv2d(x, 128, 7, 'conv1')
            pool1 = max_pool_2x2(conv1, [1, 2, 2, 1], [1, 2, 2, 1], 'VALID', 'pool1')

            conv2_a1 = conv2d(pool1, 128, 1, 'conv2_inception_a1')
            conv2_a = conv2d(conv2_a1, 128, 3, 'conv2_inception_a')
            conv2_b1 = conv2d(pool1, 128, 1, 'conv2_inception_b1')
            conv2_b = conv2d(conv2_b1, 128, 7, 'conv2_inception_b')
            conv2_c1 = conv2d(pool1, 128, 1, 'conv2_inception_c1')
            conv2 = tf.concat([conv2_a, conv2_b, conv2_c1], 3)

            pool2 = max_pool_2x2(conv2, [1, 2, 2, 1], [1, 2, 2, 1], 'VALID', 'pool2')

            conv3 = conv2d(pool2, 256, 3, 'conv3', 2)
            pool3 = max_pool_2x2(conv3, [1, 2, 2, 1], [1, 2, 2, 1], 'VALID', 'pool3')

            conv4_a1 = conv2d(pool3, 256, 1, 'conv4_inception_a1')
            conv4_a = conv2d(conv4_a1, 256, 3, 'conv4_inception_a')
            conv4_b1 = conv2d(pool3, 256, 1, 'conv4_inception_b1')
            conv4_b = conv2d(conv4_b1, 256, 7, 'conv4_inception_b')
            conv4_c1 = conv2d(pool3, 256, 1, 'conv4_inception_c1')
            conv4 = tf.concat([conv4_a, conv4_b, conv4_c1], 3)

            pool4 = max_pool_2x2(conv4, [1, 2, 2, 1], [1, 2, 2, 1], 'VALID', 'pool4')
            conv5 = conv2d(pool4, 512, 5, 'conv5', 1)
            pool5 = max_pool_2x2(conv5, [1, 2, 2, 1], [1, 2, 2, 1], 'VALID', 'pool5')

            _, shape1, shape2, shape3 = pool5.shape
            pool5_flat = tf.reshape(pool5, shape=[-1, shape1 * shape2 * shape3])

            print('conv1.shape:', conv1.shape)
            print('pool1.shape:', pool1.shape)

            print('conv2_a1.shape:', conv2_a1.shape)
            print('conv2_a.shape:', conv2_a.shape)
            print('conv2_b1.shape:', conv2_b1.shape)
            print('conv2_b.shape:', conv2_b.shape)
            print('conv2_c1.shape:', conv2_c1.shape)

            print('conv2.shape:', conv2.shape)
            print('pool2.shape', pool2.shape)

            print('conv3.shape:', conv3.shape)
            print('pool3.shape:', pool3.shape)

            print('conv4_a1.shape:', conv4_a1.shape)
            print('conv4_a.shape:', conv4_a.shape)
            print('conv4_b1.shape:', conv4_b1.shape)
            print('conv4_b.shape:', conv4_b.shape)
            print('conv4_c1.shape:', conv4_c1.shape)

            print('conv4.shape', conv4.shape)
            print('pool4.shape', pool4.shape)

            print('conv5.shape', conv5.shape)
            print('pool5.shape', pool5.shape)

            fullyconn1 = tf.layers.dense(
                pool5_flat, 128, activation=tf.nn.relu, name='fc1')
            fullyconn2 = tf.layers.dense(
                fullyconn1, 256, activation=tf.nn.relu, name='fc2')
            fullyconn3 = tf.layers.dense(fullyconn2, 512, activation=tf.nn.relu, name='fc3')
            fc3_drop = tf.layers.dropout(fullyconn3, dropout_rate_fc, training=training)
            fullyconn4 = tf.layers.dense(fc3_drop, 128, activation=tf.nn.relu, name='fc4')

            logits = tf.layers.dense(fullyconn4, n_outputs, name='output')

            print('fullyconn1.shape', fullyconn1.shape)
            print('fullyconn2.shape', fullyconn2.shape)
            print('fullyconn3.shape', fullyconn3.shape)
            print('fc3_drop.shape', fc3_drop.shape)
            print('fullyconn4.shape', fullyconn4.shape)
            print('logits.shape', logits.shape)

    return logits


def getImage(path):
    image_string = tf.read_file(path)
    image_decoded = tf.image.decode_jpeg(image_string)
    image_resized = tf.image.resize_images(image_decoded, [height, width])
    image_resized.set_shape([height, width, channels])
    image_rgb = image_resized[:, :, ::-1]
    return tf.expand_dims(image_rgb, 0)


def getImageBatch(path):
    image_string = tf.read_file(path)
    image_decoded = tf.image.decode_jpeg(image_string)
    image_resized = tf.image.resize_images(image_decoded, [height, width])
    image_resized.set_shape([height, width, channels])
    image_rgb = image_resized[:, :, ::-1]
    return image_rgb
