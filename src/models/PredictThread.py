import sys

from PyQt5.QtCore import QThread, pyqtSignal
import details as ds
import cv2
import tensorflow as tf
import neural_network_model as nnm
import os
import retrieveData as rd
from threading import Thread


class PredictThread(QThread):
    sig_done_label = pyqtSignal(str)
    sig_percent_label = pyqtSignal(int)

    def __init__(self, path, mode):
        QThread.__init__(self)
        self.path = path
        self.mode = mode
        self.__initNetwork()
        self.everything = ds.coordinates()

    def __del__(self):
        self.wait()

    def run(self):
        font = cv2.FONT_HERSHEY_SIMPLEX
        bottom_left_corner_of_text = (0, 100)
        font_scale = 2
        fontColor = (0, 0, 255)
        lineType = 4

        img = cv2.imread(self.path)
        img = cv2.resize(img, (int(len(img[0]) * 0.75), int(len(img) * 0.75)), interpolation=cv2.INTER_CUBIC)

        config = tf.ConfigProto(allow_soft_placement=True)
        if self.mode == 0 or self.mode == 2:
            with tf.Session(config=config, graph=self.g1) as sess_street:
                sess_street.run(self.street_init)

                self.saver_street.restore(sess_street, self.street_model)
                print('Model restored.')
                probabilities_street = sess_street.run(self.prediction_streets, feed_dict={
                    self.X_street: nnm.getImage(self.path).eval()})[0]
                _, max_idx_street = max([(v, i) for i, v in enumerate(probabilities_street)])

                street_name = self.street_classes[max_idx_street]
                street_confidence = probabilities_street[max_idx_street]
            cv2.putText(img, 'street: {0}'.format(street_name),
                        bottom_left_corner_of_text,
                        font,
                        font_scale,
                        fontColor,
                        lineType)
            cv2.putText(img, 'confidence: {0}%'.format(int(street_confidence * 100)),
                        (bottom_left_corner_of_text[0], bottom_left_corner_of_text[1] + 100),
                        font,
                        font_scale,
                        fontColor,
                        lineType)
        if self.mode == 1 or self.mode == 2:
            with tf.Session(config=config, graph=self.g2) as sess_segment:
                sess_segment.run(self.segment_init)

                self.saver_segment.restore(sess_segment, self.segment_model)
                print('Model restored.')
                probabilities_segment = sess_segment.run(self.prediction_segments, feed_dict={
                    self.X_segment: nnm.getImage(self.path).eval()})[0]
                _, max_idx_segment = max([(v, i) for i, v in enumerate(probabilities_segment)])

                segment_name = self.segment_classes[max_idx_segment]
                segment_confidence = probabilities_segment[max_idx_segment]
            cv2.putText(img, 'segment: {0}'.format(segment_name),
                        (bottom_left_corner_of_text[0],
                         bottom_left_corner_of_text[1] + bottom_left_corner_of_text[1] * 2 * (self.mode - 1)),
                        font,
                        font_scale,
                        fontColor,
                        lineType)
            cv2.putText(img, 'confidence: {0}%'.format(int(segment_confidence * 100)),
                        (bottom_left_corner_of_text[0],
                         bottom_left_corner_of_text[1] + 100 + bottom_left_corner_of_text[1] * 2 * (self.mode - 1)),
                        font,
                        font_scale,
                        fontColor,
                        lineType)
        if self.mode == 2:
            cv2.putText(img, '{0}'.format(self.everything[street_name][int(segment_name)]),
                        (bottom_left_corner_of_text[0], bottom_left_corner_of_text[1] * 5),
                        font,
                        font_scale,
                        fontColor,
                        lineType)
        cv2.imshow('Results', img)
        cv2.waitKey(0)

    def initNetwork(self):
        tf.reset_default_graph()
        height, width, channels = ds.get_size()

        self.g1 = tf.Graph()
        with self.g1.as_default() as g:
            with g.name_scope('street'):
                self.X_street = tf.placeholder(tf.float32, shape=[
                    1, height, width, channels], name='X')
                self.street_outputs = ds.street_nr_of_outputs()
                self.street_model = ds.best_street_saved_model()
                self.street_classes = ds.street_classes()
                logits_streets = nnm.CNN_model(self.X_street, self.street_outputs, 'street')
                self.prediction_streets = tf.nn.softmax(logits_streets)
                self.saver_street = tf.train.Saver()
                self.street_init = tf.global_variables_initializer()

        tf.reset_default_graph()
        self.g2 = tf.Graph()
        with self.g2.as_default() as g:
            with g.name_scope('segment'):
                self.X_segment = tf.placeholder(tf.float32, shape=[
                    1, height, width, channels], name='X')
                self.segment_outputs = ds.segment_nr_of_outputs()
                self.segment_model = ds.best_segment_saved_model()
                self.segment_classes = ds.segment_classes()
                logits_segments = nnm.CNN_model(self.X_segment, self.segment_outputs, 'segment')
                self.prediction_segments = tf.nn.softmax(logits_segments)
                self.saver_segment = tf.train.Saver()
                self.segment_init = tf.global_variables_initializer()

    __initNetwork = initNetwork
