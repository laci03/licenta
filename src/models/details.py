def get_size():
    height = 94
    width = 168
    channels = 3
    return height, width, channels


def training_path_street():
    """
        Returns the path to the training set for the streets
    """

    path = 'F:/Laci/Licenta/Licenta_date/processed/resized_aiud_94_168/'
    return path


def training_path_segment():
    """
        Returns the path to the training set for the segments
    """

    path = 'G:/Laci/Licenta/Licenta_date_2_CNN/processed/aiud_94_168/'
    return path


def validation_path_street():
    """
        Returns the path to the validation set for the streets
    """

    path = 'F:/Laci/Licenta/Licenta_teste/original/test_bogdan_smailo/'
    return path


def validation_path_segment():
    """Returns the path to the validation set for the segments"""

    path = 'G:/Laci/Licenta/Licenta_date_2_CNN_test/interim/test_bogdan_smailo/labeled/'
    return path


def segment_nr_of_outputs():
    return len(segment_classes())


def segment_classes_path():
    return './classes/segment_classes.txt'


def segment_classes():
    file = open(segment_classes_path(), 'r')
    classes = file.read().split('\n')
    return classes


def street_nr_of_outputs():
    return len(street_classes())


def street_classes_path():
    return './classes/street_classes.txt'


def street_classes():
    file = open(street_classes_path(), 'r')
    streets = file.read().split('\n')
    return streets


def coordinates_path():
    return 'coordinates.txt'


def coordinates():
    file = open(coordinates_path(), 'r')
    streets = file.read().split('alt')

    data = {k: [] for k in street_classes()}

    for street in streets:
        street_rows = street.split('\n')
        street_rows = [x for x in street_rows if x != '']
        street_name = street_rows[0]
        street_coordinates = street_rows[1:]

        for streetCoordinate in street_coordinates:
            aux = streetCoordinate.split(' ')[1:]
            data[street_name].append((float(aux[0]), float(aux[1])))

    return data


def segment_model_path():
    return '../saved_models/segments_saved_model/'


def street_model_path():
    return '../saved_models/streets_saved_model/'


def segment_saved_model():
    return segment_model_path() + 'awesomeness5.ckpt'


def street_saved_model():
    return street_model_path() + 'street_epoch4.ckpt'


def best_segment_saved_model():
    return best_segment_model_path() + 'best_segment.ckpt'


def best_segment_model_path():
    return segment_model_path() + 'best/'


def best_street_saved_model():
    return best_street_model_path() + 'best_street.ckpt'


def best_street_model_path():
    return street_model_path() + 'best/'


def best_accuracy_street():
    file = open(best_street_model_path() + 'best.txt', 'r')
    accuracy = float(file.read())
    return accuracy


def best_accuracy_segment():
    file = open(best_segment_model_path() + 'best.txt', 'r')
    accuracy = float(file.read())
    return accuracy
