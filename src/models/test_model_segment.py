import tensorflow as tf
import retrieveData as rd
import details as ds
import math
import numpy as np
import sys
from datetime import datetime
import neural_network_model as nnm

saved_modelsPath = ds.segment_model_path()


def test(path, model):
    (picture_files, _) = rd.retrieveLabeledDataSegments(path)
    (path, data, label, street_label) = zip(*picture_files)
    classes = ds.segment_classes()

    testing_images = [x for x in data]
    testing_paths = [x for x in path]
    testing_labels = [x for x in label]
    testing_labels_int = [int(x) for x in label]

    lookup = dict(zip(classes, range(len(classes))))
    print(lookup)

    print('testing images:', len(testing_images))

    batch_size = 16
    n_outputs = len(classes)

    tf.reset_default_graph()

    testing_dataset = tf.data.Dataset.from_tensor_slices(
        (testing_paths, testing_images, testing_labels, testing_labels_int))
    testing_dataset = testing_dataset.apply(tf.contrib.data.map_and_batch(
        map_func=rd.decodeAndResizeSegment, batch_size=tf.placeholder_with_default(
            tf.constant(batch_size, dtype=tf.int64), ()),
        num_parallel_batches=1))
    testing_dataset = testing_dataset.prefetch(batch_size)

    iterator = tf.data.Iterator.from_structure(testing_dataset.output_types,
                                               testing_dataset.output_shapes)

    testing_init_op = iterator.make_initializer(testing_dataset, name='testing_init')
    next_x, next_y = iterator.get_next()

    logits = nnm.CNN_model(next_x, n_outputs, 'segment')

    with tf.device('/gpu:0'):
        correct = tf.nn.in_top_k(logits, next_y, 1)
        accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

    config = tf.ConfigProto(allow_soft_placement=True)
    config.gpu_options.allow_growth = True
    saver = tf.train.Saver()

    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        saver.restore(sess, model)
        print('Model restored.')

        print('Testing has started')
        sess.run(testing_init_op)
        acc_test = 0
        last_percent = 0
        current_percent = 0
        counter = 0
        length = len(testing_images) / batch_size

        while True:
            try:
                current_percent = counter / length
                if last_percent * 100 + 1 < current_percent * 100:
                    print('{0}%'.format(int(current_percent * 100)), end='\r')
                    last_percent = current_percent

                acc_test += accuracy.eval()
                counter += 1
            except tf.errors.OutOfRangeError:
                print('Testing accuracy {0}'.format(acc_test / counter))
                break


if __name__ == '__main__':
    try:
        start_time = datetime.now()
        model = ds.best_segment_saved_model()
        if len(sys.argv) >= 3:
            model = sys.argv[2]
        test(sys.argv[1], model)
        print('Time taken:', datetime.now() - start_time)
    except Exception as e:
        print(
            'Sistemul s-a oprit deoarece parametrii introdusi sunt gresiti! Pentru mai multe detalii in legatura cu utilizarea scripturilor se poate consulta documentatia.')
