import tensorflow as tf
import numpy as np
import sys
import retrieveData as rd
import cv2
import os
import details as ds
import neural_network_model as nnm
from datetime import datetime

everything = ds.coordinates()


# def main(name, step, fps, path, mode = 0, output_path = './'):
#     start_time = datetime.now()
#     createVideo(name, step, fps, path, mode, output_path)
#     print('Time taken:', datetime.now() - start_time)


def main(name, step, fps, path, mode=0, output_path='./'):
    start_time = datetime.now()
    streets = os.listdir(path)
    for street in streets:
        print(street)
        if 'garii' in street or 'stadionului' in street:
            createVideo(street + '.mp4', 12, fps, path + street + '/', mode, output_path)
        # else:
        #     createVideo(street + '.mp4', step, fps, path + street + '/', mode, output_path)
    print('Time taken:', datetime.now() - start_time)


def createVideo(name, step, fps, folderPath, mode=0, output_path='./'):
    """
        0 - means streets
        1 - means segments
        2 - means both
    """
    print(folderPath)
    images = rd.retrieve_data(folderPath)
    img1 = cv2.imread(folderPath + images[0])
    videoHeight, videoWidth, _ = img1.shape
    fourcc = cv2.VideoWriter_fourcc(*'DIVX')
    video = cv2.VideoWriter(output_path + name, fourcc, fps, (videoWidth, videoHeight))

    counter = 0
    last_percent = 0
    current_percent = 0
    length = len(images)

    font = cv2.FONT_HERSHEY_SIMPLEX
    bottom_left_corner_of_text = (400, 100)
    font_scale = 3
    font_color = (0, 0, 255)
    line_type = 4

    tf.reset_default_graph()
    height, width, channels = ds.get_size()

    if mode == 0:
        X = tf.placeholder(tf.float32, shape=[
            1, height, width, channels], name='X')
        n_outputs = ds.street_nr_of_outputs()
        saved_model = ds.best_street_saved_model()
        classes = ds.street_classes()

        logits = nnm.CNN_model(X, n_outputs, 'street')
        prediction = tf.nn.softmax(logits)
        saver = tf.train.Saver()
    elif mode == 1:
        X = tf.placeholder(tf.float32, shape=[
            1, height, width, channels], name='X')
        n_outputs = ds.segment_nr_of_outputs()
        saved_model = ds.best_segment_saved_model()
        classes = ds.segment_classes()

        logits = nnm.CNN_model(X, n_outputs, 'segment')
        prediction = tf.nn.softmax(logits)
        saver = tf.train.Saver()
    else:
        g1 = tf.Graph()

        with g1.as_default() as g:
            with g.name_scope('street'):
                X_street = tf.placeholder(tf.float32, shape=[
                    1, height, width, channels], name='X')
                street_outputs = ds.street_nr_of_outputs()
                street_model = ds.best_street_saved_model()
                street_classes = ds.street_classes()
                logits_streets = nnm.CNN_model(X_street, street_outputs, 'street')
                prediction_streets = tf.nn.softmax(logits_streets)
                saver_street = tf.train.Saver()
                street_init = tf.global_variables_initializer()

        tf.reset_default_graph()
        g2 = tf.Graph()
        with g2.as_default() as g:
            with g.name_scope('segment'):
                X_segment = tf.placeholder(tf.float32, shape=[
                    1, height, width, channels], name='X')
                segment_outputs = ds.segment_nr_of_outputs()
                segment_model = ds.best_segment_saved_model()
                segment_classes = ds.segment_classes()
                logits_segments = nnm.CNN_model(X_segment, segment_outputs, 'segment')
                prediction_segments = tf.nn.softmax(logits_segments)
                saver_segment = tf.train.Saver()
                segment_init = tf.global_variables_initializer()

    if mode == 0 or mode == 1:
        config = tf.ConfigProto(allow_soft_placement=True)

        with tf.Session(config=config) as sess:
            sess.run(tf.global_variables_initializer())
            saver.restore(sess, saved_model)
            print('Model restored.')
            for image in images:
                if counter % step == 0:
                    current_percent = counter / length
                    if last_percent * 100 + 1 < current_percent * 100:
                        print('{0}%'.format(int(current_percent * 100)), end='\r')
                        last_percent = current_percent

                    img = cv2.imread(folderPath + image)

                    probabilities = sess.run(prediction, feed_dict={
                        X: nnm.getImage(folderPath + image).eval()})[0]
                    _, max_idx = max([(v, i) for i, v in enumerate(probabilities)])

                    cv2.putText(img, classes[max_idx],
                                bottom_left_corner_of_text,
                                font,
                                font_scale,
                                font_color,
                                line_type)
                    video.write(img)

                    cv2.imwrite('d:/rr/frame{0}.jpg'.format(counter), img)

                    cv2.destroyAllWindows()
                counter = counter + 1
            video.release()
    elif mode == 2:
        config = tf.ConfigProto(allow_soft_placement=True)
        with tf.Session(config=config, graph=g1) as sess_street:
            with tf.Session(config=config, graph=g2) as sess_segment:
                sess_street.run(street_init)
                sess_segment.run(segment_init)

                saver_segment.restore(sess_segment, segment_model)
                saver_street.restore(sess_street, street_model)
                print('Model restored.')

                for image in images:
                    if counter % step == 0:
                        current_percent = counter / length
                        if last_percent * 100 + 1 < current_percent * 100:
                            print('{0}%'.format(int(current_percent * 100)), end='\r')
                            last_percent = current_percent

                        img = cv2.imread(folderPath + image)
                        probabilities_street = sess_street.run(prediction_streets, feed_dict={
                            X_street: nnm.getImage(folderPath + image).eval()})[0]
                        _, max_idx_street = max([(v, i) for i, v in enumerate(probabilities_street)])

                        probabilities_segment = sess_segment.run(prediction_segments, feed_dict={
                            X_segment: nnm.getImage(folderPath + image).eval()})[0]
                        _, max_idx_segments = max([(v, i) for i, v in enumerate(probabilities_segment)])

                        street_name = street_classes[max_idx_street]
                        segment_nr = segment_classes[max_idx_segments]
                        cv2.putText(img, '{0} {1}'.format(street_name, segment_nr),
                                    bottom_left_corner_of_text,
                                    font,
                                    font_scale,
                                    font_color,
                                    line_type)

                        cv2.putText(img, '{0}'.format(everything[street_name][int(segment_nr)]),
                                    (400, 200),
                                    font,
                                    font_scale,
                                    font_color,
                                    line_type)
                        video.write(img)
                        cv2.imwrite('d:/rr/frame{0}.jpg'.format(counter), img)

                        cv2.destroyAllWindows()

                    counter = counter + 1
                video.release()

    print('Done')


if __name__ == '__main__':
    try:
        step = 30
        fps = 30.0
        path = 'F:/Laci/Licenta/Licenta_date_2_CNN_test/interim/test_bogdan_smailo/unlabeled/'
        mode = 2
        output_path = './'
        if len(sys.argv) >= 3:
            step = int(sys.argv[2])

        if len(sys.argv) >= 4:
            fps = int(sys.argv[3])

        if len(sys.argv) >= 5:
            path = sys.argv[4]

        if len(sys.argv) >= 6:
            mode = int(sys.argv[5])

        if len(sys.argv) >= 7:
            output_path = sys.argv[6]

        main(sys.argv[1], step, fps, path, mode, output_path)
    except Exception as e:
        print(
            'Sistemul s-a oprit deoarece parametrii introdusi sunt gresiti! Pentru mai multe detalii in legatura cu utilizarea scripturilor se poate consulta documentatia.')
