\contentsline {chapter}{Capitolul{} \numberline {1}Introducere - Contextul proiectului}{4}
\contentsline {section}{\numberline {1.1}Contextul proiectului}{4}
\contentsline {section}{\numberline {1.2}Motiva\c {t}ia}{5}
\contentsline {section}{\numberline {1.3}Structura lucr\u {a}rii}{5}
\contentsline {chapter}{Capitolul{} \numberline {2}Obiectivele Proiectului}{7}
\contentsline {section}{\numberline {2.1}Investigarea unei metode alternative de localizare}{7}
\contentsline {section}{\numberline {2.2}Framework pentru crearea unui clasificator}{8}
\contentsline {section}{\numberline {2.3}Dataset}{8}
\contentsline {section}{\numberline {2.4}Cerin\c {t}e Func\c {t}ionale}{8}
\contentsline {subsection}{\numberline {2.4.1}Antrenarea \c {s}i testarea unor Re\c {t}ele Convolu\c {t}ionale}{8}
\contentsline {subsection}{\numberline {2.4.2}Preg\u {a}tirea setului de date pentru antrenare}{8}
\contentsline {subsection}{\numberline {2.4.3}Predic\c {t}ii folosind modelele antrenate}{9}
\contentsline {section}{\numberline {2.5}Cerin\c {t}e Non-Func\c {t}ionale}{9}
\contentsline {subsection}{\numberline {2.5.1}Scalabilitate}{9}
\contentsline {subsection}{\numberline {2.5.2}Mentenabilitate}{9}
\contentsline {subsection}{\numberline {2.5.3}Reutilizabilitate}{9}
\contentsline {subsection}{\numberline {2.5.4}Documentare}{9}
\contentsline {subsection}{\numberline {2.5.5}Timp de r\u {a}spuns}{9}
\contentsline {chapter}{Capitolul{} \numberline {3}Studiu Bibliografic}{10}
\contentsline {section}{\numberline {3.1}Deep Learning}{10}
\contentsline {section}{\numberline {3.2}Places}{10}
\contentsline {section}{\numberline {3.3}Fruits}{12}
\contentsline {section}{\numberline {3.4}Adversarial examples}{13}
\contentsline {section}{\numberline {3.5}Dropout}{14}
\contentsline {section}{\numberline {3.6}Rectified linear unit (ReLU)}{15}
\contentsline {chapter}{Capitolul{} \numberline {4}Analiz\u {a} \c {s}i Fundamentare Teoretic\u {a}}{17}
\contentsline {section}{\numberline {4.1}Tehnologii utilizate}{17}
\contentsline {subsection}{\numberline {4.1.1}Python}{17}
\contentsline {subsection}{\numberline {4.1.2}Tensorflow}{18}
\contentsline {section}{\numberline {4.2}Convolutional Neural Network}{18}
\contentsline {subsection}{\numberline {4.2.1}Convolutional Layer}{19}
\contentsline {subsection}{\numberline {4.2.2}Pooling Layer}{19}
\contentsline {subsection}{\numberline {4.2.3}Feedforward Fully connected Layer}{19}
\contentsline {subsection}{\numberline {4.2.4}Dropout}{20}
\contentsline {subsection}{\numberline {4.2.5}Backpropagation}{21}
\contentsline {subsection}{\numberline {4.2.6}Learning Rate}{21}
\contentsline {subsection}{\numberline {4.2.7}Mini-Batch Gradient Descent}{22}
\contentsline {subsection}{\numberline {4.2.8}AdamOptimizer}{23}
\contentsline {subsection}{\numberline {4.2.9}Inception}{24}
\contentsline {chapter}{Capitolul{} \numberline {5}Proiectare de Detaliu \c {s}i Implementare}{25}
\contentsline {section}{\numberline {5.1}Arhitectur\u {a} propus\u {a}}{25}
\contentsline {section}{\numberline {5.2}Dataset}{27}
\contentsline {section}{\numberline {5.3}Framework}{34}
\contentsline {section}{\numberline {5.4}Features}{37}
\contentsline {subsection}{\numberline {5.4.1}Extragerea cadrelor din video}{37}
\contentsline {subsection}{\numberline {5.4.2}Redenumirea imaginilor dintr-un folder}{38}
\contentsline {subsection}{\numberline {5.4.3}Redimensionarea cadrelor dintr-un set de date}{39}
\contentsline {section}{\numberline {5.5}Model}{40}
\contentsline {subsection}{\numberline {5.5.1}Creare de histograme}{40}
\contentsline {subsection}{\numberline {5.5.2}Creare de video}{41}
\contentsline {subsection}{\numberline {5.5.3}Libr\u {a}rie cu date constante}{42}
\contentsline {subsection}{\numberline {5.5.4}Re\c {t}eaua Convolu\c {t}ional\u {a}}{43}
\contentsline {subsection}{\numberline {5.5.5}Predic\c {t}ie pe baza imaginilor}{43}
\contentsline {subsection}{\numberline {5.5.6}\^{I}nc\u {a}rcarea imaginilor}{44}
\contentsline {subsection}{\numberline {5.5.7}Testarea modelelor}{45}
\contentsline {subsection}{\numberline {5.5.8}Antrenarea re\c {t}elelor}{45}
\contentsline {subsection}{\numberline {5.5.9}Interfa\c {t}\u {a} utilizator}{47}
\contentsline {chapter}{Capitolul{} \numberline {6}Testare \c {s}i Validare}{49}
\contentsline {section}{\numberline {6.1}Acurate\c {t}e}{49}
\contentsline {section}{\numberline {6.2}Timpul necesar pentru \^{\i }nv\u {a}\c {t}are}{49}
\contentsline {section}{\numberline {6.3}Fruits 360}{50}
\contentsline {section}{\numberline {6.4}Outputul re\c {t}elelor}{50}
\contentsline {section}{\numberline {6.5}Distribu\c {t}ia predic\c {t}iilor}{51}
\contentsline {chapter}{Capitolul{} \numberline {7}Manual de Instalare \c {s}i Utilizare}{53}
\contentsline {section}{\numberline {7.1}Instalare}{53}
\contentsline {subsection}{\numberline {7.1.1}Python}{53}
\contentsline {subsection}{\numberline {7.1.2}CUDA}{53}
\contentsline {subsection}{\numberline {7.1.3}cuDNN}{54}
\contentsline {subsection}{\numberline {7.1.4}Tensorflow}{54}
\contentsline {section}{\numberline {7.2}Utilizare}{54}
\contentsline {subsection}{\numberline {7.2.1}Extragere cadre din video}{54}
\contentsline {subsection}{\numberline {7.2.2}Redenumirea cadrelor}{55}
\contentsline {subsection}{\numberline {7.2.3}Testarea re\c {t}elelor}{55}
\contentsline {subsection}{\numberline {7.2.4}Crearea unui video}{56}
\contentsline {subsection}{\numberline {7.2.5}Predic\c {t}ii}{57}
\contentsline {subsection}{\numberline {7.2.6}Crearea histogramelor}{58}
\contentsline {subsection}{\numberline {7.2.7}Redimensionarea cadrelor}{59}
\contentsline {subsection}{\numberline {7.2.8}Antrenarea re\c {t}elelor Convolu\c {t}ionale}{60}
\contentsline {subsection}{\numberline {7.2.9}Interfa\c {t}a utilizator}{61}
\contentsline {chapter}{Capitolul{} \numberline {8}Concluzii}{64}
\contentsline {section}{\numberline {8.1}Dezvolt\u {a}ri ulterioare}{65}
\contentsline {subsection}{\numberline {8.1.1}Recurrent Neural Networks(RNNs)}{65}
\contentsline {subsection}{\numberline {8.1.2}Filtre \c {s}i zgomot}{65}
\contentsline {subsection}{\numberline {8.1.3}Aplicarea cuno\c {s}tiin\c {t}elor despre str\u {a}zi}{65}
\contentsline {subsection}{\numberline {8.1.4}Cre\c {s}terea num\u {a}rului de str\u {a}zi \c {s}i a preciziei}{65}
\contentsline {chapter}{Bibliografie}{66}
\contentsline {chapter}{Anexa{} \numberline {A}Sec\c {t}iuni relevante din cod}{68}
\contentsline {chapter}{Anexa{} \numberline {B}Alte informa\c {t}ii relevante}{69}
\contentsline {chapter}{Anexa{} \numberline {C}Lucr\u {a}ri publicate}{70}
